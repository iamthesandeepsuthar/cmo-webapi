﻿namespace CMOWebApi.Core.Enums
{
    public class DdlKeysEnum
    {
        public enum DdlKeys
        {
            [StringValue("Entry")]
            ddlEntry,

            [StringValue("Department")]
            ddlDepartment,

            [StringValue("Department")]
            ddlDepartmentAward,

            [StringValue("AdminDepartmentAward")]
            ddlAdminDepartmentAward,

            [StringValue("DepartmentScheme")]
            ddlDepartmentScheme,

            [StringValue("AdminDepartmentScheme")]
            ddlAdminDepartmentScheme,

            [StringValue("DepartmentOrder")]
            ddlDepartmentOrder,
            [StringValue("AdminDepartmentOrder")]
            ddlAdminDepartmentOrder,

            [StringValue("ddlAdminDepartmentAchievements")]
            ddlAdminDepartmentAchievements,
            [StringValue("DepartmentAchievement")]
            ddlDepartmentAchievement,

            [StringValue("AdminDepartmentImportantDecision")]
            ddlAdminDepartmentImportantDecision,

            [StringValue("DepartmentImportantDecision")]
            ddlDepartmentImportantDecision,

            [StringValue("AdminDepartmentWebsite")]
            ddlAdminDepartmentWebsite,
            [StringValue("DepartmentWebsite")]
            ddlDepartmentWebsite,


            [StringValue("AdminDepartmentMobileApplication")]
            ddlAdminDepartmentMobileApplication,

            [StringValue("DepartmentMobileApplication")]
            ddlDepartmentMobileApplication,


            [StringValue("AdminDepartmentCMSpeech")]
            ddlAdminDepartmentCMSpeech,
            [StringValue("DepartmentCMSpeech")]
            ddlDepartmentCMSpeech,


            [StringValue("AdminDepartmentProgramVideo")]
            ddlAdminDepartmentProgramVideo,
            [StringValue("DepartmentProgramVideo")]
            ddlDepartmentProgramVideo,


            [StringValue("AdminDepartmentServicesOfferedbyDepartment")]
            ddlAdminDepartmentServicesOfferedbyDepartment,
            [StringValue("DepartmentServicesOfferedbyDepartment")]
            ddlDepartmentServicesOfferedbyDepartment,

            [StringValue("AdminDepartmentLetterstoCentralGovt")]
            ddlAdminDepartmentLetterstoCentralGovt,
            [StringValue("DepartmentLetterstoCentralGovt")]
            ddlDepartmentLetterstoCentralGovt,

            [StringValue("BeneficiaryType")]
            ddlBeneficiaryType,

            [StringValue("ExecutiveDepartment")]
            ddlExecutiveDepartment,

            [StringValue("ConcernedDepartment")]
            ddlConcernedDepartment,

            [StringValue("Nodel")]
            ddlNodel,

            [StringValue("SchemeType")]
            ddlSchemeType,

            [StringValue("SchemeOutput")]
            ddlSchemeOutput,

            [StringValue("ModeofPayment")]
            ddlModeofPayment,

            [StringValue("PaymentThrough")]
            ddPaymentThrough,

            [StringValue("Service")]
            ddlService,

            [StringValue("Scheme")]
            ddlScheme,


            [StringValue("Category")]
            ddlCategory,

            [StringValue("Jankalyan Category")]
            ddlJankalyanCategory,

            [StringValue("PaymentDisbursementMode")]
            ddlPaymentDisbursementMode,

            [StringValue("ModeOfDelivery")]
            ddlModeOfDelivery,

            [StringValue("SchemeArea")]
            ddlSchemeArea,

            [StringValue("District")]
            ddlDistrict,

            [StringValue("Sector")]
            ddlSector,

            [StringValue("Sector")]
            ddlImpCategory,

            [StringValue("BeneficiaryCategory")]
            ddlBeneficiaryCategory,

            [StringValue("OrderSector")]
            ddlOrderSector,

            [StringValue("OrderType")]
            ddlOrderType,

            [StringValue("ddlGenerateOrderType")]
            ddlGenerateOrderType,

            [StringValue("OrderIssueBy")]
            ddlOrderIssueBy,

            [StringValue("OrderModuleName")]
            ddlOrderModuleName,

            [StringValue("OrderRelatedToYear")]
            ddlOrderRelatedToYear,

            [StringValue("OrderRelatedToDepartment")]
            ddlOrderRelatedToDepartment,

            [StringValue("SchemeMaster")]
            ddlSchemeMaster,

            [StringValue("SchemeMaster")]
            ddlSchemefaqMaster,


            [StringValue("LookUpType")]
            ddlLookUpType,

            [StringValue("AdvCategory")]
            ddlAdvCategory,

            [StringValue("AdvSubCategory")]
            ddlAdvSubCategory,

            [StringValue("UserType")]
            ddlUserType,

            [StringValue("Title")]
            ddlTitle,

            [StringValue("NameTitle")]
            ddlNameTitle,

            [StringValue("Designation")]
            ddlDesignation,

            [StringValue("UserGroup")]
            ddlUserGroup,

            [StringValue("AdminDepartment")]
            ddlAdminDepartment,

            [StringValue("Division")]
            ddlDivision,

            [StringValue("ParliamentConstituency")]
            ddlParliamentConstituency,

            [StringValue("AssemblyConstituency")]
            ddlAssemblyConstituency,

            [StringValue("Referencee")]
            ddlReferencee,

            [StringValue("Block")]
            ddlBlock,

            [StringValue("Tehsil")]
            ddlTehsil,

            [StringValue("Gender")]
            ddlGender,

            [StringValue("User")]
            ddlUser,

            [StringValue("ApplicationType")]
            ddlApplicationType,

            [StringValue("PageType")]
            ddlPageType,

            [StringValue("ApplicationMenu")]
            ddlApplicationMenu,

            [StringValue("AdvertisementNotification")]
            ddlAdvertisementNotification,

            [StringValue("PlatformMaster")]
            ddlPlatformMaster,
            [StringValue("WidgetMaster")]
            ddlWidgetMaster,

            [StringValue("CommonMaster")]
            ddlCommonMaster,

            [StringValue("Scheme/Service Type")]
            ddlSchemeServiceType,

            [StringValue("Scheme/Service Program Area")]
            ddlSchemeServiceProgramArea,

            [StringValue("Scheme/Service Eligibility Criteria")]
            ddlSchemeEligibility,

            [StringValue("Scheme/Service List Of Required Doc")]
            ddlSchemeListOfRequiredDoc,

            [StringValue("Scheme/Service How to pay fees")]
            ddlSchemePayFees,

            [StringValue("Scheme/Service Mode Of Delivery")]
            ddlSchemeModeOfDelivery,

            [StringValue("Scheme/Service Payment Disbursement Frequency")]
            ddlSchemePaymentDisbursementFrequency,

            [StringValue("Scheme/Service Mode Of Disbursement")]
            ddlSchemeModeOfDisbursement,

            [StringValue("Scheme/Service List Of Other Doc")]
            ddlSchemeListOfOtherDoc,

            [StringValue("Scheme/Service Name Of Document")]
            ddlSchemeNameOfDocument,

            [StringValue("Monitoring Parameters")]
            ddlMonitoringParameters,

            [StringValue("Office")]
            ddlOffice,

            [StringValue("Notification Template Type")]
            ddlNotificationTemplateType,

            [StringValue("LMS_LetterAction")]
            ddlLMS_LetterAction,

            [StringValue("VIPLMS_LetterAction")]
            ddlVIPLMS_LetterAction,

            [StringValue("ddlURLType")]
            ddlURLType,

            [StringValue("ddlHelpDocType")]
            ddlHelpDocType,

            [StringValue("ddlSchemeName")]
            ddlSchemeName,

            [StringValue("ddlTableName")]
            ddlTableName,

            [StringValue("ddlOtherByLookUp")]
            ddlOtherByLookUp,

            [StringValue("ddlOtherByLookUp")]
            ddlMonitoringParameterLookUpType,

            [StringValue("ddlUserByAdminDepartment")]
            ddlUserByAdminDepartment,

            [StringValue("ddlSchemePageType")]
            ddlSchemePageType,

            [StringValue("ddlAuthoritySignatory")]
            ddlAuthoritySignatory,

            [StringValue("ddlCCReference")]
            ddlCCReference,

            [StringValue("ddlCitizenLetterAttachment")]
            ddlCitizenLetterAttachment,

            [StringValue("ddlContactPersonType")]
            ddlContactPersonType,
            [StringValue("ddlAchievementCategory")]
            ddlAchievementCategory,
            [StringValue("ddlAchievementSubCategory")]
            ddlAchievementSubCategory,
            [StringValue("ddlImportantDecisionSubCategory")]
            ddlImportantDecisionSubCategory,

            [StringValue("ddluserlist")]
            ddluserlist,
            [StringValue("ddlordersubtype")]
            ddlOrderSubType,
            [StringValue("ddlSchemes")]
            ddlSchemes,
            [StringValue("ddlGalleryUploadType")]
            ddlGalleryUploadType,
            [StringValue("ddlCancellationReason")]
            ddlCancellationReason,
            [StringValue("ddlCreateVCList")]
            ddlCreateVCList,
            [StringValue("ddlVCParticipantCategory")]
            ddlVCParticipantCategory,
            [StringValue("ddlVCMode")]
            ddlVCMode,

            [StringValue("Vc Creation type")]
            ddlVCType,

            [StringValue("Current Date VC")]
            ddlCurrentDateVC,

            [StringValue("Client Module")]
            ddlClientModule,

            [StringValue("State")]
            ddlState,

            [StringValue("Entry Module")]
            ddlEntryTypeModule,

            [StringValue("Priority Module")]
            ddlPriorityModule,

            [StringValue("Module")]
            ddlModuleName,

            [StringValue("Compliant Action Module")]
            ddlCompliantAction,

            [StringValue("Compliant Filter Module")]
            ddlCompliantFilter,

            [StringValue("AchievementDepartment")]
            ddlAchievementDepartment,

            [StringValue("OrderWithRequiredType")]
            OrderWithRequiredType,

            [StringValue("ddlEntryTypeMaster")]
            ddlEntryTypeMaster,

            [StringValue("ddlSSOId")]
            ddlSSOId,

            [StringValue("ddlDptContactDesignation")]
            ddlDptContactDesignation,

            [StringValue("ddlDepartmentCategory")]
            ddlDepartmentCategory,

            [StringValue("ddlDepartmentListByCategory")]
            ddlDepartmentListByCategory,
            [StringValue("ddlCMISModule")]

            ddlCMISModule,

            [StringValue("ddlProjectStatus")]
            ddlProjectStatus,

            [StringValue("ddlProjectProgram")]
            ddlProjectProgram,

            [StringValue("ddlProjectYearOfInitiation")]
            ddlProjectYearOfInitiation,

            [StringValue("ddlProjectCategory")]
            ddlProjectCategory,

            [StringValue("ddlProjectSubCategory")]
            ddlProjectSubCategory,
            [StringValue("ddlProjectSubSubCategory")]
            ddlProjectSubSubCategory,

            [StringValue("ddlSubCategoryAchievement")]
            ddlSubCategoryAchievement,
            [StringValue("ddlSubCategoryCMSpeech")]
            ddlSubCategoryCMSpeech,
            [StringValue("ddlSubCategoryImportantDecision")]
            ddlSubCategoryImportantDecision,
            [StringValue("ddlSubCategoryAward")]
            ddlSubCategoryAward,
            [StringValue("ddlSubCategoryProgramVideo")]
            ddlSubCategoryProgramVideo,
            [StringValue("ddlSubCategoryLetterstoCentralGovt")]
            ddlSubCategoryLetterstoCentralGovt,
            [StringValue("ddlSubCategoryServicesOfferedbyDepartment")]
            ddlSubCategoryServicesOfferedbyDepartment,
            [StringValue("ddlSubCategoryMobileApplication")]

            ddlSubCategoryMobileApplication,
            [StringValue("ddlSubCategoryWebsite")]

            ddlSubCategoryWebsite,

            [StringValue("ddlMLAConstituency")]
            ddlMLAConstituency,

            [StringValue("ddlMPConstituency")]
            ddlMPConstituency,

            [StringValue("ProjectMileStoneStatus")]
            ddlProjectMileStoneStatus,

            [StringValue("ProjectMileStone")]
            ddlProjectMileStone,

            [StringValue("CMISAchievementAdmDepartment")]
            ddlCMISAchievementAdmDepartment,

            [StringValue("CMISAchievementDepartment")]

            ddlCMISAchievementDepartment,

            [StringValue("Newspaper News Mode")]
            ddlNewspaperNewsMode,

            [StringValue("Newspaper News Type")]
            ddlNewspaperNewsType,

            [StringValue("Newspaper Souce Type")]
            ddlNewspaperSourceType,

            [StringValue("Newspaper Subject")]
            ddlNewspaperSubject,

            [StringValue("Newspaper Master")]
            ddlNewspaperMaster,

            [StringValue("Newspaper classification")]
            ddlNewspaperClassification,

            [StringValue("Newspaper Publication Type")]
            ddlNewspaperPublicationType,

            [StringValue("Newspaper Edition")]
            ddlNewspaperEdition,

            [StringValue("Newspaper Page Number")]
            ddlNewspaperPageNumber,

            [StringValue("Newspaper Progress News Type")]
            ddlNewspaperProgressNewsType,

            [StringValue("Newspaper  Coverage Type")]
            ddlNewspaperCoverageType,

            [StringValue("ddlProjectWorkCategory")]
            ddlProjectWorkCategory,

            [StringValue("ddlProjectProgramSchemeType")]
            ddlProjectProgramSchemeType,
            [StringValue("ddlExistingProjectAdmDepartment")]
            ddlExistingProjectAdmDepartment,

            [StringValue("ddlExistingProjectDepartment")]
            ddlExistingProjectDepartment,
            [StringValue("ddlExistingProjectWorkType")]
            ddlExistingProjectWorkType,

            [StringValue("ddlExistingProjectStatus")]
            ddlExistingProjectStatus,

            [StringValue("ddlExistingProjectSector")]
            ddlExistingProjectSector,

            [StringValue("ddlExistingProjectCategory")]
            ddlExistingProjectCategory,

            [StringValue("ddlExistingProjectSubCategory")]
            ddlExistingProjectSubCategory,
            [StringValue("ddlExistingProjectSchemeProgram")]

            ddlExistingProjectSchemeProgram,

            [StringValue("ddlExistingProjectStartYear")]
            ddlExistingProjectStartYear,

            [StringValue("ddlExistingProjectMLAConstituency")]
            ddlExistingProjectMLAConstituency,

            [StringValue("ddlExistingProjectMPConstituency")]
            ddlExistingProjectMPConstituency,
            [StringValue("ddlExistingProjectMLADistrict")]

            ddlExistingProjectMLADistrict,

            [StringValue("ddlExistingNewsAdmDepartment")]
            ddlExistingNewsAdmDepartment,

            [StringValue("ddlExistingNewsDepartment")]
            ddlExistingNewsDepartment,

            [StringValue("ddlExistingNewsSubject")]
            ddlExistingNewsSubject,

            [StringValue("ddlExistingNewsPaper")]
            ddlExistingNewsPaper,

            [StringValue("ddlExistingNewsPublicationType")]
            ddlExistingNewsPublicationType,



            [StringValue("ddlExistingNewsCoverageType")]
            ddlExistingNewsCoverageType,

            [StringValue("ChildPageDescriptionCategory")]
            ddlChildPageDescriptionCategory,

            [StringValue("PageMasterDetails")]
            ddlPageMasterDetails,

            [StringValue("ddlCMISReportModules")]
            ddlCMISReportModules,

            [StringValue("ddlDepartmentForCMISReport")]
            ddlDepartmentForCMISReport,

            [StringValue("ddlMLAConstituencyWithDistrictName")]
            ddlMLAConstituencyWithDistrictName,

            [StringValue("ddlCreatedByUserForProject")]
            ddlCreatedByUserForProject,

            [StringValue("ddlDepartmentPhotoGallery")]
            ddlDepartmentPhotoGallery,

            [StringValue("ddlAdminDepartmentPhotoGallery")]
            ddlAdminDepartmentPhotoGallery,

            [StringValue("Employment")]
            ddlDepartmentEmployment,

            [StringValue("ddlAdminDepartmentEmployment")]
            ddlAdminDepartmentEmployment,

            [StringValue("ddlDepartmentDepartmentalPhotoGallery")]
            ddlDepartmentDepartmentalPhotoGallery,

            [StringValue("ddlAdminDepartmentDepartmentalPhotoGallery")]
            ddlAdminDepartmentDepartmentalPhotoGallery,

            [StringValue("ddlMLAConstituencyWithDesignation")]
            ddlMLAConstituencyWithDesignation,

            [StringValue("ddlCMISBudgetYear")]
            ddlCMISBudgetYear,

            [StringValue("ddlVCCategory")]
            ddlVCCategory,

            [StringValue("ddlCMOOfficers")]
            ddlCMOOfficers,

			[StringValue("ddlKPICategory")]
			ddlKPICategory,

			[StringValue("ddlPhysicalUnit")]
			ddlPhysicalUnit,

			[StringValue("ddlPhysicalUnit")]
			ddlMonth,

			[StringValue("ddlFinancialUnit")]
			ddlFinancialUnit,

			[StringValue("ddlYearMaster")]
			ddlYearMaster,

            [StringValue("PageMasterforCMISCompliance")]
            ddlPageMasterforCMISCompliance,

            [StringValue("ddlComparativeYearGrandTotal")]
            ddlComparativeYearGrandTotal,

            [StringValue("ddlUserDepartment")]
            ddlUserDepartment
        }

        public enum RadioButtonKeys
        {
            [StringValue("Scheme/Service Radio OwnedBy")]
            RadioSchemeOwnedBy,

            [StringValue("Scheme/Service Radio RGDPS Act")]
            RadioRGDPSAct,

            [StringValue("Scheme/Service Radio Apply For Scheme")]
            RadioApplyForScheme,

            [StringValue("Scheme/Service Radio Scheme Expried On")]
            RadioSchemeExpriedOn,

            [StringValue("Scheme/Service Radio Mode of Applying")]
            RadioSchemeModeofApplying,

            [StringValue("Scheme/Service Radio Service Fee")]
            RadioServiceFee,

            [StringValue("Scheme/Service Radio Payment Disbursement Frequency")]
            RadioPaymentDisbursementFrequency,

            [StringValue("Scheme/Service Radio List Of Required Doc")]
            RadioListOfRequiredDoc,

            [StringValue("Made Of Appling Online And Both")]
            RadioMadeOfApplingOnlineAndBoth,

            [StringValue("Advertisement IsPull or IsPush")]
            RadioIsPushOrIsPull,

            [StringValue("Linked To Scheme(If Any)")]
            RadioLinkedToScheme,

            [StringValue("Scheme Type")]
            RadioSchemeType,

            [StringValue("Office Type")]
            RadioOfficeType,

            [StringValue("VC Location Type")]
            RadioVcLocationType,

            [StringValue("Scheme/Service Radio")]
            RadioSchemeService,

            [StringValue("Radio Projects UrbanorRural")]
            RadioProjectsUrbanorRural,

			[StringValue("department/district radio")]
			RadioDepartmentDistrict,

			[StringValue("department/Category radio")]
			RadioDepartmentCategory


		}

        public enum MonitoringParametresKeys
        {
            [StringValue("Department Master")]
            tblDepartmentMaster,

            [StringValue("Category Master")]
            tblCategoryMaster,

            [StringValue("Beneficiary Cagegory")]
            tblBeneficiaryCagegory,

            [StringValue("Get Other By MP Lookup")]
            tblMonitoringParameterLookup

        }

        public enum MonitoringParametresUpdateStatusKeys
        {
            [StringValue("Update IsActive for Monitoring Parametres Mapping")]
            ParamMapping,

            [StringValue("Update IsDeleted for Monitoring Parametres Entry and Value table")]
            ParamValueEntry,

        }
        public enum GalleryUploadType
        {
            [StringValue("PHOTO")]
            Photo,
            [StringValue("VIDEO")]
            Video,
            [StringValue("YOUTUBE URL")]
            YoutubeUrl
        }

    }
}
