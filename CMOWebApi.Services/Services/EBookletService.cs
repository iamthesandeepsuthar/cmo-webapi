﻿using AutoMapper;
using CMOWebApi.Core;
using CMOWebApi.Data;
using CMOWebApi.Data.UnitOfWork;
using CMOWebApi.Models.ComparetiveModel;
using CMOWebApi.Services.IServices;
using CMOWebApi.Services.ServiceHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMOWebApi.Services.Services
{
	public class EBookletService : BaseService , IEbookletService
	{
		#region /// Variable ///

		IUnitofWork _uow;

		#endregion

		#region /// Constructor  ///

		public EBookletService(IUnitofWork uow)
		{
			_uow = uow;
		}

		#endregion

		#region /// Methods ///

		/// <summary>
		/// Get E-booklet by Beneficiary Category and Department
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		public ServiceResponse<EBookletResponseModel> GeEbooklet(EBookletFilterModel model)
		{
			ServiceResponse<EBookletResponseModel> objReturn = new ServiceResponse<EBookletResponseModel>();
			try
			{
				if (model.DepartmentCode>0)
				{
					objReturn.Data =_uow.GenericRepository<tblDepartmentMaster>().GetAll(filter: f=>f.DepartmentCode==model.DepartmentCode).Select(x => new EBookletResponseModel { Name = x.DepartmentTitle, Logo = x.YoutubeURL }).FirstOrDefault();
				}
				else if(model.BeneficiaryCategoryCode>0)
				{
					objReturn.Data = _uow.GenericRepository<tblBeneficiaryCagegory>().GetAll(filter: f => f.cm_ansmtcategoryid == model.BeneficiaryCategoryCode).Select(x => new EBookletResponseModel { Name = x.ansmtcategory, Logo = x.AttachmentURL }).FirstOrDefault();
				}
                else if (model.KPICategoryCode > 0)
                {
                    objReturn.Data = _uow.GenericRepository<tblCPT_KPICategoryMaster>().GetAll(filter: f => f.Code == model.KPICategoryCode).Select(x => new EBookletResponseModel { Name = x.Name, Logo = x.Name }).FirstOrDefault(); //TODO LOGO
                }
                List<SP_CPT_E_BookletReport_Result> data = _uow.ExeccuteStoreProcedure<SP_CPT_E_BookletReport_Result>("SP_CPT_E_BookletReport @DepartmentCode ,@BeneficiaryCategoryCode ,@KPICategoryCode,@YearWiseCode,@GrandTotalCode,@GeneralEntryEBookletCode"
                 , new SqlParameter("DepartmentCode", SqlDbType.Int) { Value = model.DepartmentCode > 0 ? model.DepartmentCode : 0 }
				 , new SqlParameter("BeneficiaryCategoryCode", SqlDbType.Int) { Value = model.BeneficiaryCategoryCode > 0 ? model.BeneficiaryCategoryCode : 0 }
				 , new SqlParameter("KPICategoryCode", SqlDbType.Int) { Value = model.KPICategoryCode > 0 ? model.KPICategoryCode : 0 }
                  , new SqlParameter("YearWiseCode", SqlDbType.BigInt) { Value = model.YearWiseCode > 0 ? model.YearWiseCode : 0 }
                  , new SqlParameter("GrandTotalCode", SqlDbType.BigInt) { Value = model.GrandTotalCode > 0 ? model.GrandTotalCode : 0 }
                    , new SqlParameter("GeneralEntryEBookletCode", SqlDbType.Int) { Value = model.GeneralEntryEBookletCode > 0 ? model.GeneralEntryEBookletCode : 0 }
                 ).ToList();

				var config = new MapperConfiguration(cfg =>
				{
					cfg.CreateMap<SP_CPT_E_BookletReport_Result, BookletDataModel>();
				});
				IMapper mapper = config.CreateMapper();
				objReturn.Data.EBookletList = mapper.Map(data, objReturn.Data.EBookletList);

				return SetResultStatus(objReturn.Data, MessageStatus.Save, true);
			}
			catch (Exception ex)
			{
				objReturn.Data = null;
				return SetResultStatus<EBookletResponseModel>(null, MessageStatus.Error, false);
			}
		}

		#endregion
	}



}
