//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CMOWebApi.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class vwOfficeWithDistrict
    {
        public long OfficeId { get; set; }
        public Nullable<int> OfficeCode { get; set; }
        public Nullable<int> DistrictCode { get; set; }
        public string DistrictTitle { get; set; }
        public string DistrictShortTitle { get; set; }
        public string DistrictTitleHindi { get; set; }
        public string DistrictShortTitleHindi { get; set; }
        public string OfficeName { get; set; }
        public string OfficeNameHindi { get; set; }
        public string OfficeAddress { get; set; }
        public string OfficeShortName { get; set; }
        public string OfficeShortNameHindi { get; set; }
    }
}
