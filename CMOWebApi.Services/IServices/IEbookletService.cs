﻿using CMOWebApi.Models.ComparetiveModel;
using CMOWebApi.Services.ServiceHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMOWebApi.Services.IServices
{
	public interface IEbookletService
	{
		/// <summary>
		/// Get E-booklet by Beneficiary Category and Department
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		ServiceResponse<EBookletResponseModel> GeEbooklet(EBookletFilterModel model);
	}
}
