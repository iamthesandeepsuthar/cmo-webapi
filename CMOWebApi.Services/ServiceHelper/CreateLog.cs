﻿using System;
using System.Configuration;
using System.IO;
using System.Web;

namespace CMOWebApi.Services.ServiceHelper
{
    public class CreateLogHelper
    {
        public static void CreateLogFile(string strMsg)
        {
            try
            {
                StreamWriter log;
                var path = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["LogFilesPath"]) + "logfile-" + DateTime.Now.ToString("MM-dd-yyyy") + ".txt";


                if (!File.Exists(path))
                {
                    File.Create(path).Close();
                }

                log = System.IO.File.AppendText(path);
                log.WriteLine(strMsg);
                log.Close();
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
