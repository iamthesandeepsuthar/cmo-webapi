
/***************************************************

Created By :Tanmaya 

Script Date : 18-12-2019

Description : Get MPR of scheme

exec [spSCM_GetMPRByScheme] 6

****************************************************/

alter proc [dbo].[spSCM_GetMPRByScheme](
-- Optional Filters for Dynamic Search

	@SchemeId	bigint=0
)
as 
begin

Select mp.*, scm.ShortNameHindi, scm.NameHindi, scm.ShortNameEnglish, scm.NameEnglish, scm.[Description]
from
(select  MAX(SchemeId) as SchemeId, YEAR(YearMonth) AS [Year], MONTH(YearMonth) as Months,FORMAT(YearMonth, 'MMMM  yyyy') as [MonthName], Count(Id) as MPRCount, MAX(YearMonth) as YearMonth
from [tblSCM_MonitoringParameterDataEntry] where SchemeId=@SchemeId
group by YEAR(YearMonth), MONTH(YearMonth),FORMAT(YearMonth, 'MMMM  yyyy')
) as mp
inner join tblSCM_SchemeMaster as scm on mp.SchemeId=scm.Id
where scm.IsDeleted=0 and scm.Id=@SchemeId

end
