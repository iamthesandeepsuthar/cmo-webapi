//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CMOWebApi.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblODR_GNRT_DepartmentSetupMaster
    {
        public long Id { get; set; }
        public Nullable<long> Code { get; set; }
        public Nullable<int> DepartmentCode { get; set; }
        public string Address1 { get; set; }
        public string AddressHindi1 { get; set; }
        public string Address2 { get; set; }
        public string AddressHindi2 { get; set; }
        public string Address3 { get; set; }
        public string AddressHindi3 { get; set; }
        public string FooterLine1 { get; set; }
        public string FooterLineHindi1 { get; set; }
        public string FooterLine2 { get; set; }
        public string FooterLineHindi2 { get; set; }
        public string Logo1 { get; set; }
        public string Logo2 { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<int> OfficeCode { get; set; }
        public Nullable<long> FileSize { get; set; }
        public string FacebookUrl { get; set; }
        public string Twitter { get; set; }
        public string Youtube { get; set; }
        public string Backgroundcolor { get; set; }
        public string SerialNumber { get; set; }
        public Nullable<bool> IsAutoEmail { get; set; }
        public Nullable<bool> IsAutoSMS { get; set; }
    }
}
