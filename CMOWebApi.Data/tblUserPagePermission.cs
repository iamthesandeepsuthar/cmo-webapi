//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CMOWebApi.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblUserPagePermission
    {
        public long UserPagePermissionId { get; set; }
        public int UserId { get; set; }
        public long PageCode { get; set; }
        public string ApplicationCode { get; set; }
        public bool AddPermission { get; set; }
        public bool EditPermission { get; set; }
        public bool DeletePermission { get; set; }
        public bool ViewPermission { get; set; }
        public string UserType { get; set; }
    }
}
