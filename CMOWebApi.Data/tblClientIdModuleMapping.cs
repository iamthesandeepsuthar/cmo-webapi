//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CMOWebApi.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblClientIdModuleMapping
    {
        public long Id { get; set; }
        public Nullable<long> ClientIdForService { get; set; }
        public Nullable<long> ModuleCode { get; set; }
    
        public virtual tblClientIdForService tblClientIdForService { get; set; }
    }
}
