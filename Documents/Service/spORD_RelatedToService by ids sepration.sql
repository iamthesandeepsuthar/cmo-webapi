USE [CMIS_APP]
GO
/****** Object:  StoredProcedure [dbo].[spORD_RelatedToService]    Script Date: 18-12-2019 12:35:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****************************************
Created By :Tanmaya 
Script Date : 17-12-2019
Description : To get Related to Data
				by Order ids
exec [spORD_RelatedToService] 
*********************************** ******/

 ALTER proc  [dbo].[spORD_RelatedToService]

 @Ids nvarchar(max)=''

as

Begin

DECLARE @url NVARCHAR(100)='' 

SELECT item into #ControlTable  FROM dbo.fnSplit(@Ids,',')

select * from vwODR_OrderRelatedTo where OrderEntryID in (select * from #ControlTable)

drop table #ControlTable

end
