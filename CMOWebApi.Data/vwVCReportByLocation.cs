//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CMOWebApi.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class vwVCReportByLocation
    {
        public long Id { get; set; }
        public Nullable<long> SNo { get; set; }
        public string Name { get; set; }
        public string Designation { get; set; }
        public string mode { get; set; }
        public string Location { get; set; }
        public Nullable<int> DisplayOrder { get; set; }
    }
}
