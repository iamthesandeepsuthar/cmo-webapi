﻿using System;

namespace CMOWebApi.Core.Enums
{
    public class FixedValues
    {
        public enum DBFilterType
        {
            [StringValue("int")]
            Int,
            [StringValue("decimal")]
            Decimal,
            [StringValue("numberfrom")]
            Number_From,
            [StringValue("numberto")]
            Number_To,
            [StringValue("multiselect")]
            MultiSelect,
            [StringValue("date")]
            Date,
            [StringValue("datefrom")]
            Date_From,
            [StringValue("dateto")]
            Date_To,
            [StringValue("bit")]
            Bit,
            [StringValue("text")]
            Text,
            [StringValue("varchar")]
            VarChar,
            [StringValue("nvarchar")]
            nVarChar,
            [StringValue("datefrom_multidatestring")]
            DateFrom_MultiDateString,
            [StringValue("dateto_multidatestring")]
            DateTo_MultiDateString,
        }

    }
}
