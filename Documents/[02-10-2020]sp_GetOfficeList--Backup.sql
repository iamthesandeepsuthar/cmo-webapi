


 -- sp_GetOfficeList 1,0,0,'प्रशासनिक ','',' ',0 ,'12-17-2018','12-07-2020'  
ALTER  PROC [dbo].[sp_GetOfficeList]       
(      
 @UserId INT=0 ,  
 @DepartmentCode int =0,  
 @OfficeCode int =0,  
 @OfficeNameHindi nvarchar(500)='',  
 @OfficeShortName nvarchar(500)='',  
 @KeywordSearch nvarchar(max)='',   
 @Activeview  bit = 1,  
 @CreatedFrom nvarchar(50)='12-17-2018',  
 @CreatedTo nvarchar(50) = ''     
)      
AS      
BEGIN       
     if(@CreatedTo = '')  
begin  
SET @CreatedTo=  CONVERT(varchar(10) , getdate(),110)   
   END
      
  DECLARE @UserType NVARCHAR(25)    
  DECLARE @User_DepartmentCodes NVARCHAR(MAX)=''
  DECLARE @User_GroupCode NVARCHAR(10)=''       
  DECLARE @User_DivisionCodes NVARCHAR(MAX)=''       
  DECLARE @User_DistrictCodes NVARCHAR(MAX)=''       
  DECLARE @User_TehsilCodes NVARCHAR(MAX)=''       
  DECLARE @User_BlockCodes NVARCHAR(MAX)=''         
  DECLARE @User_ACCodes NVARCHAR(MAX)='' 
  
  
 SELECT @UserType=ISNULL(UserType,''), @User_DepartmentCodes=ISNULL(DepartmentCodes,''), @User_GroupCode=ISNULL(GroupCode,''), @User_DivisionCodes=ISNULL(DivisionCodes,''),       
 @User_DistrictCodes=ISNULL(DistrictCodes,''), @User_TehsilCodes=ISNULL(TehsilCodes,''), @User_BlockCodes=ISNULL(BlockCodes,''), @User_ACCodes=ISNULL(ACCodes,'')      
 FROM vwUserDetail WHERE UserId=@UserId       
      
 DECLARE @strQuery NVARCHAR(4000)      
          
 SET @strQuery=' select * from  (SELECT * FROM vwOfficeMaster   '  +  case when ( @User_DepartmentCodes  <>  '')   then +' where  DepartmentCode in  (' + @User_DepartmentCodes + ') ' else '' END  +' ) UM
 where 1= 1' 
 +  case when (@DepartmentCode  <>  0)   then +' AND DepartmentCode = ' +  cast (@DepartmentCode as nvarchar) else '' END
 +  case when ( @OfficeCode  <>  0)   then +' AND OfficeCode  = ' + cast (@OfficeCode as nvarchar) else '' END
 +  case when ( @OfficeNameHindi  <>  '')   then +' AND OfficeNameHindi like  N''%' +LTRIM(RTRIM(cast(@OfficeNameHindi as nvarchar)))+'%''' else '' END
 +  case when ( @OfficeShortName  <>  '')   then +' AND OfficeShortName like  ''%' + cast (@OfficeShortName as nvarchar) +'%''' else '' END
 +  ' AND UM.IsActive  = ' + Cast (@Activeview as varchar(2))    

  +  CASE WHEN  (@CreatedFrom <> '' AND  @CreatedTo <> '' )  THEN ' AND   (convert (date, UM.CreatedDate,110)  Between   ''' + CONVERT(varchar(10) , @CreatedFrom ,110)  + ''' AND ''' +  CONVERT(varchar(10) , @CreatedTo ,110) + ''' ) ' ELSE '' END    
    +  CASE WHEN  @KeywordSearch <> '' THEN ' AND  ( UM.OfficeName  like ''%' +  LTRIM(Rtrim(@KeywordSearch)) +'%'' OR UM.Department  like ''%'   
    + LTRIM(Rtrim(@KeywordSearch)) +'%'' OR UM.OfficeNameHindi  like ''%' + LTRIM(Rtrim(@KeywordSearch)) +'%'  
      +   '%'' OR UM.OfficeShortName  like ''%' + LTRIM(Rtrim(@KeywordSearch)) +'%''   
     )'  ELSE '' END 

	 PRINT (@strQuery)
	 EXEC (@strQuery)
 END


