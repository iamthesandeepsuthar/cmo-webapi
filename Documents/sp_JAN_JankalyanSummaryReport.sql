


/********************************************     
 Created By: Tanmaya  
 Created Date: 12-08-2020   
 Description: Get summary report of all module.
  
 exec [sp_JAN_JankalyanSummaryReport] 4,-1
 ********************************************/  
ALTER PROC [dbo].[sp_JAN_JankalyanSummaryReport]
(    
@DepartmentCode int =0 
,@Status int =1 
)    
    
as     
Begin    
     
Begin --[Scheme] (Group-1/Individual-2)

declare @IndividualScheme bigint=0, @GroupScheme bigint=0

set @GroupScheme= (select count(Id) from tblSCM_SchemeMaster as sm
left join tblDepartmentMaster as dm on dm.DepartmentCode=sm.NodelDepartmentCode
where sm.PageType=1 and sm.IsDeleted=0 and dm.DepartmentIsActive=1 AND dm.DepartmentIsDeleted=0 
and isnull(sm.NodelDepartmentCode,0)=case when @DepartmentCode>0 then @DepartmentCode else isnull(sm.NodelDepartmentCode,0) end
and isnull(sm.IsActive,0)= case when isnull(@Status,-1)= -1 then isnull(sm.IsActive,0) else @Status end ) 

set @IndividualScheme= (select count(Id) from tblSCM_SchemeMaster as sm
left join tblDepartmentMaster as dm on dm.DepartmentCode=sm.NodelDepartmentCode
where sm.PageType=2 and sm.IsDeleted=0 and dm.DepartmentIsActive=1 AND dm.DepartmentIsDeleted=0 
and isnull(sm.NodelDepartmentCode,0)=case when @DepartmentCode>0 then @DepartmentCode else isnull(sm.NodelDepartmentCode,0) end
and isnull(sm.IsActive,0)= case when isnull(@Status,-1)= -1 then isnull(sm.IsActive,0) else @Status end ) 

end --[Scheme] 

Begin --[Government Document]
declare @Order bigint=0, @Circular bigint=0,  @Notification bigint=0,  @PolicyGuidelines bigint=0,  @ActRules bigint=0,  
@CitizenCharter bigint=0,  @COVID19 bigint=0, @AnnualProgressReport bigint=0, @Publication bigint=0, @Budget bigint=0

--Order
set @Order= (select count(Id) from tblODR_OrderEntryMaster as om
left join tblDepartmentMaster as dm on dm.DepartmentCode=om.DepartmentCode
where om.Type=1 and om.IsDeleted=0 and dm.DepartmentIsActive=1 AND dm.DepartmentIsDeleted=0 
and isnull(om.DepartmentCode,0)=case when @DepartmentCode>0 then @DepartmentCode else isnull(om.DepartmentCode,0) end
and isnull(om.IsActive,0)= case when isnull(@Status,-1)= -1 then isnull(om.IsActive,0) else @Status end )

--Circular
set @Circular= (select count(Id) from tblODR_OrderEntryMaster as om
left join tblDepartmentMaster as dm on dm.DepartmentCode=om.DepartmentCode
where om.Type=2 and om.IsDeleted=0 and dm.DepartmentIsActive=1 AND dm.DepartmentIsDeleted=0 
and isnull(om.DepartmentCode,0)=case when @DepartmentCode>0 then @DepartmentCode else isnull(om.DepartmentCode,0) end
and isnull(om.IsActive,0)= case when isnull(@Status,-1)= -1 then isnull(om.IsActive,0) else @Status end )

--Notification
set @Notification= (select count(Id) from tblODR_OrderEntryMaster as om
left join tblDepartmentMaster as dm on dm.DepartmentCode=om.DepartmentCode
where om.Type=3 and om.IsDeleted=0 and dm.DepartmentIsActive=1 AND dm.DepartmentIsDeleted=0 
and isnull(om.DepartmentCode,0)=case when @DepartmentCode>0 then @DepartmentCode else isnull(om.DepartmentCode,0) end
and isnull(om.IsActive,0)= case when isnull(@Status,-1)= -1 then isnull(om.IsActive,0) else @Status end )

--Policy & Guidelines
set @PolicyGuidelines= (select count(Id) from tblODR_OrderEntryMaster as om
left join tblDepartmentMaster as dm on dm.DepartmentCode=om.DepartmentCode
where om.Type=4 and om.IsDeleted=0 and dm.DepartmentIsActive=1 AND dm.DepartmentIsDeleted=0 
and isnull(om.DepartmentCode,0)=case when @DepartmentCode>0 then @DepartmentCode else isnull(om.DepartmentCode,0) end
and isnull(om.IsActive,0)= case when isnull(@Status,-1)= -1 then isnull(om.IsActive,0) else @Status end )

--Act & Rules
set @ActRules= (select count(Id) from tblODR_OrderEntryMaster as om
left join tblDepartmentMaster as dm on dm.DepartmentCode=om.DepartmentCode
where om.Type=5 and om.IsDeleted=0 and dm.DepartmentIsActive=1 AND dm.DepartmentIsDeleted=0 
and isnull(om.DepartmentCode,0)=case when @DepartmentCode>0 then @DepartmentCode else isnull(om.DepartmentCode,0) end
and isnull(om.IsActive,0)= case when isnull(@Status,-1)= -1 then isnull(om.IsActive,0) else @Status end )

--Citizen Charter
set @CitizenCharter= (select count(Id) from tblODR_OrderEntryMaster as om
left join tblDepartmentMaster as dm on dm.DepartmentCode=om.DepartmentCode
where om.Type=6 and om.IsDeleted=0 and dm.DepartmentIsActive=1 AND dm.DepartmentIsDeleted=0 
and isnull(om.DepartmentCode,0)=case when @DepartmentCode>0 then @DepartmentCode else isnull(om.DepartmentCode,0) end
and isnull(om.IsActive,0)= case when isnull(@Status,-1)= -1 then isnull(om.IsActive,0) else @Status end )

--COVID-19 Orders
set @COVID19= (select count(Id) from tblODR_OrderEntryMaster as om
left join tblDepartmentMaster as dm on dm.DepartmentCode=om.DepartmentCode
where Type=8 and IsDeleted=0 and dm.DepartmentIsActive=1 AND dm.DepartmentIsDeleted=0 
and isnull(om.DepartmentCode,0)=case when @DepartmentCode>0 then @DepartmentCode else isnull(om.DepartmentCode,0) end
and isnull(om.IsActive,0)= case when isnull(@Status,-1)= -1 then isnull(om.IsActive,0) else @Status end )

--Annual Progress Report
set @AnnualProgressReport= (select count(Id) from tblODR_OrderEntryMaster as om
left join tblDepartmentMaster as dm on dm.DepartmentCode=om.DepartmentCode
where om.Type=9 and om.IsDeleted=0 and dm.DepartmentIsActive=1 AND dm.DepartmentIsDeleted=0 
and isnull(om.DepartmentCode,0)=case when @DepartmentCode>0 then @DepartmentCode else isnull(om.DepartmentCode,0) end
and isnull(om.IsActive,0)= case when isnull(@Status,-1)= -1 then isnull(om.IsActive,0) else @Status end )

--Publication 
set @Publication= (select count(Id) from tblODR_OrderEntryMaster as om
left join tblDepartmentMaster as dm on dm.DepartmentCode=om.DepartmentCode
where Type=10 and IsDeleted=0 and dm.DepartmentIsActive=1 AND dm.DepartmentIsDeleted=0 
and isnull(om.DepartmentCode,0)=case when @DepartmentCode>0 then @DepartmentCode else isnull(om.DepartmentCode,0) end
and isnull(om.IsActive,0)= case when isnull(@Status,-1)= -1 then isnull(om.IsActive,0) else @Status end )

--Budget  
set @Budget = (select count(Id) from tblODR_OrderEntryMaster as om
left join tblDepartmentMaster as dm on dm.DepartmentCode=om.DepartmentCode
where Type=11 and IsDeleted=0 and dm.DepartmentIsActive=1 AND dm.DepartmentIsDeleted=0 
and isnull(om.DepartmentCode,0)=case when @DepartmentCode>0 then @DepartmentCode else isnull(om.DepartmentCode,0) end
and isnull(om.IsActive,0)= case when isnull(@Status,-1)= -1 then isnull(om.IsActive,0) else @Status end )

end --[Government Document] 

Begin --[Achievement]

declare @Advertisement bigint=0, @Awards bigint=0, @DepartmentalAchievements bigint=0, @Video bigint=0, 
@Audio bigint=0, @ImportantDecisions bigint=0, @CabinetDecisions bigint=0, @Posters bigint=0

--Advertisement 
set @Advertisement= (select count(Id) from tblADV_Achievements as ac
left join tblDepartmentMaster as dm on dm.DepartmentCode=ac.DepartmentCode
where ac.AchievementCategoryCode=12  and ac.IsDeleted=0 and dm.DepartmentIsActive=1 AND dm.DepartmentIsDeleted=0 
and isnull(ac.DepartmentCode,0)=case when @DepartmentCode>0 then @DepartmentCode else isnull(ac.DepartmentCode,0) end
and isnull(ac.IsActive,0)= case when isnull(@Status,-1)= -1 then isnull(ac.IsActive,0) else @Status end ) 

--Videos 
set @Video= (select count(Id) from tblADV_Achievements as ac
left join tblDepartmentMaster as dm on dm.DepartmentCode=ac.DepartmentCode
where ac.AchievementCategoryCode=2  and ac.IsDeleted=0 and dm.DepartmentIsActive=1 AND dm.DepartmentIsDeleted=0 
and isnull(ac.DepartmentCode,0)=case when @DepartmentCode>0 then @DepartmentCode else isnull(ac.DepartmentCode,0) end
and isnull(ac.IsActive,0)= case when isnull(@Status,-1)= -1 then isnull(ac.IsActive,0) else @Status end ) 

--Departmental Achievements 
set @DepartmentalAchievements= (select count(Id) from tblADV_Achievements  as ac
left join tblDepartmentMaster as dm on dm.DepartmentCode=ac.DepartmentCode
where ac.AchievementCategoryCode=1 and ac.IsDeleted=0 and dm.DepartmentIsActive=1 AND dm.DepartmentIsDeleted=0 
and isnull(ac.DepartmentCode,0)=case when @DepartmentCode>0 then @DepartmentCode else isnull(ac.DepartmentCode,0) end
and isnull(ac.IsActive,0)= case when isnull(@Status,-1)= -1 then isnull(ac.IsActive,0) else @Status end ) 

--Important Decisions 
set @ImportantDecisions= (select count(Id) from tblADV_Achievements as ac
left join tblDepartmentMaster as dm on dm.DepartmentCode=ac.DepartmentCode
where ac.AchievementCategoryCode=13 and ac.IsDeleted=0 and dm.DepartmentIsActive=1 AND dm.DepartmentIsDeleted=0 
and isnull(ac.DepartmentCode,0)=case when @DepartmentCode>0 then @DepartmentCode else isnull(ac.DepartmentCode,0) end
and isnull(ac.IsActive,0)= case when isnull(@Status,-1)= -1 then isnull(ac.IsActive,0) else @Status end ) 

--Audio
set @Audio= (select count(Id) from tblADV_Achievements as ac
left join tblDepartmentMaster as dm on dm.DepartmentCode=ac.DepartmentCode
where ac.AchievementCategoryCode=11 and ac.IsDeleted=0 and dm.DepartmentIsActive=1 AND dm.DepartmentIsDeleted=0 
and isnull(ac.DepartmentCode,0)=case when @DepartmentCode>0 then @DepartmentCode else isnull(ac.DepartmentCode,0) end
and isnull(ac.IsActive,0)= case when isnull(@Status,-1)= -1 then isnull(ac.IsActive,0) else @Status end )

--Poster
set @Posters= (select count(Id) from tblADV_Achievements  as ac
left join tblDepartmentMaster as dm on dm.DepartmentCode=ac.DepartmentCode
where ac.AchievementCategoryCode=10 and ac.IsDeleted=0 and dm.DepartmentIsActive=1 AND dm.DepartmentIsDeleted=0 
and isnull(ac.DepartmentCode,0)=case when @DepartmentCode>0 then @DepartmentCode else isnull(ac.DepartmentCode,0) end
and isnull(ac.IsActive,0)= case when isnull(@Status,-1)= -1 then isnull(ac.IsActive,0) else @Status end )

--Awards
set @Awards= (select count(Id) from tblADV_Achievements as ac
left join tblDepartmentMaster as dm on dm.DepartmentCode=ac.DepartmentCode
where ac.AchievementCategoryCode=3 and ac.IsDeleted=0 and dm.DepartmentIsActive=1 AND dm.DepartmentIsDeleted=0 
and isnull(ac.DepartmentCode,0)=case when @DepartmentCode>0 then @DepartmentCode else isnull(ac.DepartmentCode,0) end
and isnull(ac.IsActive,0)= case when isnull(@Status,-1)= -1 then isnull(ac.IsActive,0) else @Status end )

--Cabinet Decisions
set @CabinetDecisions= (select count(Id) from tblADV_Achievements  as ac
left join tblDepartmentMaster as dm on dm.DepartmentCode=ac.DepartmentCode
where ac.AchievementCategoryCode=14 and ac.IsDeleted=0 and dm.DepartmentIsActive=1 AND dm.DepartmentIsDeleted=0 
and isnull(ac.DepartmentCode,0)=case when @DepartmentCode>0 then @DepartmentCode else isnull(ac.DepartmentCode,0) end
and isnull(ac.IsActive,0)= case when isnull(@Status,-1)= -1 then isnull(ac.IsActive,0) else @Status end )


end --[Achievement]


Begin --[Final Result]
SELECT TotalCount,ModuleName from (  
select @IndividualScheme as [Schemes/Services(Individual Schemes)], @GroupScheme as [Schemes/Services(Group Schemes)] --end [Scheme] 
,@Order as [Order], @Circular as [Circular], @Notification as [Notification], @PolicyGuidelines as [Policy & Guidelines]
,@ActRules as [Act & Rules], @CitizenCharter as [Citizen Charter], @COVID19 as [COVID-19 Orders], @AnnualProgressReport as [Annual Progress Report]
,@Publication as [Publication], @Budget as [Budget] --end [Government Document]
,@Advertisement as [Advertisement], @ImportantDecisions as [Important Decisions], @Audio as [Audio], @Posters as [Poster]
,@Awards as [Awards], @CabinetDecisions as [Cabinet Decisions], @DepartmentalAchievements as [Departmental Achievements]
,@Video as [Videos] --end [Achievement]
) as ModuleTable
UNPIVOT
(
  TotalCount
  for ModuleName in (
   [Schemes/Services(Individual Schemes)], [Schemes/Services(Group Schemes)],[Order], [Circular], [Notification], [Policy & Guidelines], [Act & Rules], [Citizen Charter]
  ,[COVID-19 Orders], [Annual Progress Report], [Publication], [Budget], [Advertisement], [Important Decisions],[Audio], [Poster], [Awards]
  ,[Cabinet Decisions], [Departmental Achievements],[Videos]
  )
) ModuleUnpivot;

end --[Final Result]

end 
