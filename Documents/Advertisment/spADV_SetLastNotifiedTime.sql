USE [CMIS_APP]
GO
/****** Object:  StoredProcedure [dbo].[spADV_SetLastNotifiedTime]    Script Date: 23-10-2019 14:15:56 ******/


/***************************************************
Created By :Tanmaya 
Script Date : 17-09-2019
Description : Set Last notified time for every 
				advertisement and Approval master
exec [spADV_SetLastNotifiedTime]
****************************************************/

 ALTER proc [dbo].[spADV_SetLastNotifiedTime]
  @key nvarchar(100)='',
  @NotificationIds nvarchar(max)=''
 as
 Begin

   SELECT item into #ControlTable  FROM dbo.fnSplit(@NotificationIds,',')
 
  if(@key='ADV')
 Begin

  update tblADV_NotificationMaster set LastNotifed=GETDATE() where id in(Select * from #ControlTable)
  end
   else 
  if(@key='ADMIN')
 Begin
  update tblADV_ApprovalDetailMaster set LastNotifed=GETDATE();
  end
end


