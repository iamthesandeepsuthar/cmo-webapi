USE [CMIS_APP]
GO
/****** Object:  StoredProcedure [dbo].[spSCM_MonitoringParameterMonthlyRecordByScheme]    Script Date: 23-12-2019 10:32:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***************************************************
Created By :Tanmaya 
Script Date : 14-10-2019
Description : Get Monitoring parameter Data of a 
				particular month by month and SchemeId
exec [spSCM_MonitoringParameterMonthlyRecordByScheme] 45,10,2037
****************************************************/

 ALTER proc [dbo].[spSCM_MonitoringParameterMonthlyRecordByScheme]
  @SchemeId bigint=0,
  @Month int =0,
  @Year int=0
 as
 Begin
 
  declare @maxMonth int, @secondMax int


  SELECT @maxMonth=MAX(MONTH(YearMonth)) 
  FROM tblSCM_MonitoringParameterDataEntry WHERE SchemeId=@SchemeId 

  SELECT @secondMax=MAX(MONTH(YearMonth))  
  FROM tblSCM_MonitoringParameterDataEntry 
  WHERE SchemeId=@SchemeId AND MONTH(YearMonth)<>@maxMonth 



 select MappingTableName as FieldName,pde.FieldId as MonitoringParamId ,FieldValue,DataEntryId,FORMAT(YearMonth, 'MMMM  yyyy') as [MonthName],
dm.DepartmentTitle as FieldDisplayValue,pde.Id as DataEntryFieldValueId,
(CASE WHEN (MONTH(YearMonth)=@maxMonth OR MONTH(YearMonth)=@secondMax) THEN 1 ELSE 0 END) AS IsEdit 
from [tblSCM_MonitoringParameterDataEntryFieldValue] as pde
inner join tblSCM_MonitoringParameterDataEntry as mpde on mpde.Id=pde.DataEntryId
inner join tblSCM_MonitoringParameterMaster as pm on pde.FieldId=pm.Id
left join tblDepartmentMaster as dm on pde.FieldValue= dm.DepartmentCode
where pde.SchemeId=@SchemeId and pm.Type='DDL' and pm.MappingTableName='tblDepartmentMaster'
--and  MONTH(YearMonth)=@Month and Year(YearMonth)=@Year
and MONTH(YearMonth) = case when @Month >0 then @Month else MONTH(YearMonth) end
and Year(YearMonth) = case when @Year >0 then @Year else Year(YearMonth) end

union

 select MappingTableName as FieldName,pde.FieldId as MonitoringParamId ,FieldValue,DataEntryId,FORMAT(YearMonth, 'MMMM  yyyy') as [MonthName],
bc.ansmtcategory as FieldDisplayValue,pde.Id as DataEntryFieldValueId,
(CASE WHEN (MONTH(YearMonth)=@maxMonth OR MONTH(YearMonth)=@secondMax) THEN 1 ELSE 0 END) AS IsEdit 
from [tblSCM_MonitoringParameterDataEntryFieldValue] as pde
inner join tblSCM_MonitoringParameterDataEntry as mpde on mpde.Id=pde.DataEntryId
inner join tblSCM_MonitoringParameterMaster as pm on pde.FieldId=pm.Id
left join tblBeneficiaryCagegory as bc on  pde.FieldValue=  bc.cm_ansmtcategoryid
where pde.SchemeId=@SchemeId and pm.Type='DDL' and pm.MappingTableName='tblBeneficiaryCagegory'
--and  MONTH(YearMonth)=@Month and Year(YearMonth)=@Year
and MONTH(YearMonth) = case when @Month >0 then @Month else MONTH(YearMonth) end
and Year(YearMonth) = case when @Year >0 then @Year else Year(YearMonth) end

union

 select MappingTableName as FieldName,pde.FieldId as MonitoringParamId ,FieldValue,DataEntryId,FORMAT(YearMonth, 'MMMM  yyyy') as [MonthName],
cm.Category as FieldDisplayValue,pde.Id as DataEntryFieldValueId,
(CASE WHEN (MONTH(YearMonth)=@maxMonth OR MONTH(YearMonth)=@secondMax) THEN 1 ELSE 0 END) AS IsEdit 
from [tblSCM_MonitoringParameterDataEntryFieldValue] as pde
inner join tblSCM_MonitoringParameterDataEntry as mpde on mpde.Id=pde.DataEntryId
inner join tblSCM_MonitoringParameterMaster as pm on pde.FieldId=pm.Id
left join tblCategoryMaster as cm on  pde.FieldValue=  cm.CategoryCode
where pde.SchemeId=@SchemeId and pm.Type='DDL' and pm.MappingTableName='tblCategoryMaster'
--and  MONTH(YearMonth)=@Month and Year(YearMonth)=@Year
and MONTH(YearMonth) = case when @Month >0 then @Month else MONTH(YearMonth) end
and Year(YearMonth) = case when @Year >0 then @Year else Year(YearMonth) end
union

 select MappingTableName as FieldName,pde.FieldId as MonitoringParamId ,FieldValue,DataEntryId,FORMAT(YearMonth, 'MMMM  yyyy') as [MonthName],
FieldValue as FieldDisplayValue,pde.Id as DataEntryFieldValueId,
(CASE WHEN (MONTH(YearMonth)=@maxMonth OR MONTH(YearMonth)=@secondMax) THEN 1 ELSE 0 END) AS IsEdit 
from [tblSCM_MonitoringParameterDataEntryFieldValue] as pde
inner join tblSCM_MonitoringParameterDataEntry as mpde on mpde.Id=pde.DataEntryId
inner join tblSCM_MonitoringParameterMaster as pm on pde.FieldId=pm.Id
where pde.SchemeId=@SchemeId and pm.Type<>'DDL' 
--and  MONTH(YearMonth)=@Month and Year(YearMonth)=@Year
and MONTH(YearMonth) = case when @Month >0 then @Month else MONTH(YearMonth) end
and Year(YearMonth) = case when @Year >0 then @Year else Year(YearMonth) end
union

 select MappingTableName as FieldName,pde.FieldId as MonitoringParamId ,FieldValue,DataEntryId,FORMAT(YearMonth, 'MMMM  yyyy') as [MonthName],
dm.Name as FieldDisplayValue,pde.Id as DataEntryFieldValueId,
(CASE WHEN (MONTH(YearMonth)=@maxMonth OR MONTH(YearMonth)=@secondMax) THEN 1 ELSE 0 END) AS IsEdit 
from [tblSCM_MonitoringParameterDataEntryFieldValue] as pde
inner join tblSCM_MonitoringParameterDataEntry as mpde on mpde.Id=pde.DataEntryId
inner join tblSCM_MonitoringParameterMaster as pm on pde.FieldId=pm.Id
left join tblMonitoringParameterLookup as dm on pde.FieldValue= dm.Id
where pde.SchemeId=@SchemeId and pm.Type='DDL' and pm.MappingTableName like '%tblMonitoringParameterLookup%'
--and  MONTH(YearMonth)=@Month and Year(YearMonth)=@Year
and MONTH(YearMonth) = case when @Month >0 then @Month else MONTH(YearMonth) end
and Year(YearMonth) = case when @Year >0 then @Year else Year(YearMonth) end
end


