﻿namespace CMOWebApi.Models.AdminModel.MasterModel
{
	public class SchemeTypeViewModel
	{
		public long? Id { get; set; }
		public string Name { get; set; }
		public string NameHindi { get; set; }
		public bool? IsActive { get; set; }
		public bool? IsDelete { get; set; }
	}
}
