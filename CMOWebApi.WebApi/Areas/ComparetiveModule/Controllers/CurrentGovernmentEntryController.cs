﻿using CMOWebApi.Core;
using CMOWebApi.Models.ComparetiveModel;
using CMOWebApi.Models.GeneralModel;
using CMOWebApi.Services.IServices;
using CMOWebApi.Services.ServiceHelper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace CMOWebApi.WebAPI.Areas.ComparetiveModule.Controllers
{
    public class CurrentGovernmentEntryController : ApiController
    {
        #region /// Variable ///

        private readonly ICurrentGovernmentEntryService _CurrentGovernmentEntryService;

        #endregion

        #region /// Constructor ///

        public CurrentGovernmentEntryController(ICurrentGovernmentEntryService CurrentGovernmentEntryService)
        {
            _CurrentGovernmentEntryService = CurrentGovernmentEntryService;
        }

        #endregion

        #region /// Method ///

        /// <summary>
        /// Get all KPI category Master
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>     
        [HttpPost]
        public IHttpActionResult Get(IndexModel model)
        {
            ServiceResponse<PagedData<CurrentGovtEntryListViewModel>> objReturn = new ServiceResponse<PagedData<CurrentGovtEntryListViewModel>>();
            try
            {
                return Ok(_CurrentGovernmentEntryService.GetAll(model));
            }
            catch (Exception ex)
            {
                objReturn.IsSuccess = false;
                objReturn.Message = MessageStatus.Error;
                objReturn.Data = null;
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Project category by Id(Primary key)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>       
        [HttpGet]
        public IHttpActionResult Get(long id)
        {
            ServiceResponse<CurrentGovtEntryModel> objReturn = new ServiceResponse<CurrentGovtEntryModel>();
            try
            {
                if (id >= 0)
                {
                    return Ok(_CurrentGovernmentEntryService.GetById(id));
                }
                else
                {
                    objReturn.IsSuccess = false;
                    objReturn.Message = MessageStatus.InvalidData;
                    return Ok(objReturn);
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Craete new KPI category Master
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>      
        [HttpPost]
        public async Task<IHttpActionResult> Post(CurrentGovtEntryModel model)
        {
            ServiceResponse<string> objReturn = new ServiceResponse<string>();
            try
            {
                if (ModelState.IsValid)
                {
                    //if (!_CurrentGovernmentEntryService.IsDataAvailable(model.YearCode, model.MonthCode, model.Id))
                    //{
                        return Ok(await _CurrentGovernmentEntryService.Create(model));
                    //}
                    //else
                    //{
                    //    objReturn.IsSuccess = false;
                    //    objReturn.Message = MessageStatus.YearMonthExist;
                    //    return Ok(objReturn);
                    //}
                }
                else
                {
                    IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                    objReturn.Data = JsonConvert.SerializeObject(allErrors);
                    objReturn.IsSuccess = false;
                    objReturn.Message = MessageStatus.InvalidData;
                    return Ok(objReturn);
                }
            }
            catch (Exception ex)
            {
                objReturn.IsSuccess = false;
                objReturn.Message = MessageStatus.Error;
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Update existing KPI category Master
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IHttpActionResult> Put(CurrentGovtEntryModel model)
        {
            ServiceResponse<string> objReturn = new ServiceResponse<string>();
            try
            {
                if (ModelState.IsValid)
                {
                    //if (!_CurrentGovernmentEntryService.IsDataAvailable(model.YearCode, model.MonthCode, model.Id))
                    //{
                        return Ok(await _CurrentGovernmentEntryService.Edit(model));
                    //}
                    //else
                    //{
                    //    objReturn.IsSuccess = false;
                    //    objReturn.Message = MessageStatus.YearMonthExist;
                    //    return Ok(objReturn);
                    //}
                }
                else
                {
                    IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                    objReturn.Data = JsonConvert.SerializeObject(allErrors);
                    objReturn.IsSuccess = false;
                    objReturn.Message = MessageStatus.InvalidData;
                    return Ok(objReturn);
                }
            }
            catch (Exception ex)
            {
                objReturn.IsSuccess = false;
                objReturn.Message = MessageStatus.Error;
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Update Status(Active/De-Active)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>       
        [HttpGet]
        public async Task<IHttpActionResult> UpdateActiveStatus(long id)
        {
            ServiceResponse<string> objReturn = new ServiceResponse<string>();
            try
            {
                if (id >= 0)
                {
                    return Ok(await _CurrentGovernmentEntryService.UpdateActiveStatus(id));
                }
                else
                {
                    objReturn.IsSuccess = false;
                    objReturn.Message = MessageStatus.InvalidData;
                    return Ok(objReturn);
                }
            }
            catch (Exception ex)
            {
                objReturn.IsSuccess = false;
                objReturn.Message = MessageStatus.Error;
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Check dublicate records by year and month
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>      
        [HttpPost]
        public IHttpActionResult IsDuplicateData(CurrentGovtEntryModel model)
        {
            ServiceResponse<CurrentGovtEntryModel> objReturn = new ServiceResponse<CurrentGovtEntryModel>();
            try
            {
                return Ok(_CurrentGovernmentEntryService.IsDataAvailable(model));
            }
            catch (Exception ex)
            {
                objReturn.IsSuccess = false;
                objReturn.Message = MessageStatus.Error;
                return InternalServerError(ex);
            }
        }

        #endregion
    }
}
