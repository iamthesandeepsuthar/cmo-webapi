//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CMOWebApi.Data
{
    using System;
    
    public partial class sp_GetImportantDecisionList_Result
    {
        public int Id { get; set; }
        public string Achievement { get; set; }
        public string AchievementHindi { get; set; }
        public Nullable<int> AchievementCategoryCode { get; set; }
        public string CMOComments { get; set; }
        public string KeyWord { get; set; }
        public string AutoKeyWord { get; set; }
        public Nullable<int> DepartmentCode { get; set; }
        public string Description { get; set; }
        public string DescriptionHindi { get; set; }
        public int Priority { get; set; }
        public string PdfFIleName { get; set; }
        public string Url { get; set; }
        public Nullable<System.DateTime> AchievementDate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public string UserName { get; set; }
        public string Department { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsVisible { get; set; }
        public Nullable<long> AchievementSubCategoryCode { get; set; }
        public string ImagePath { get; set; }
        public string AchievementCategory { get; set; }
        public string AchievementCategoryHindi { get; set; }
        public string AchievementSubCategory { get; set; }
        public string AchievementSubCategoryHindi { get; set; }
        public string AchievementSubCategoryImagePath { get; set; }
        public string DepartmentHindi { get; set; }
    }
}
