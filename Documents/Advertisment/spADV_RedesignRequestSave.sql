
/****************************************
Created By :Tanmaya 
Script Date : 12-09-2019
Description : To save Multiple New Design
				and return Admin approval detail 
				for notify to Admin

exec [spADV_RedesignRequestSave] 1,'85f4e531-45ae-4673-992f-9f41a40ec39b.jpg'
*********************************** ******/

 ALTER proc  [dbo].[spADV_RedesignRequestSave]

 @RedesignId bigint=0,
 @Urls nvarchar(max)=''

as

Begin

DECLARE @url NVARCHAR(100)='' 

SELECT item into #ControlTable  FROM dbo.fnSplit(@Urls,',')

--select * from  #ControlTable
--delete from tblADV_RedesignRequestByPlatformUserLookup where RedesignPlatformUserLookupId=@RedesignId

update tblADV_RedesignRequestByPlatformUserLookup set IsNew=0 where RedesignPlatformUserLookupId=@RedesignId

while exists (select * from #ControlTable)
begin

    select top 1 @url = item
    from #ControlTable
    order by item asc
 
 update [tblADV_RedesignPlatformUserLookup] set IsApproved=0 ,IsRejected=0,Remaks=null
 where id=@RedesignId

	insert into tblADV_RedesignRequestByPlatformUserLookup values(@RedesignId,1,@url)

    delete #ControlTable
    where item = @url

end

drop table #ControlTable



select Id,Code,Name,NameHindi,Email,MobileNo,DefaultMobileNo,DefaultEmail
,NotificationPeriod,IsActive from tblADV_ApprovalDetailMaster
where IsDelete=0 and IsActive=1



end

