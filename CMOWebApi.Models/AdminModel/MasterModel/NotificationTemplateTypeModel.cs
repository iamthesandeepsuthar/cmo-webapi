﻿namespace CMOWebApi.Models.AdminModel.MasterModel
{
    public class NotificationTemplateTypeModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string NameHindi { get; set; }
    }
}
