//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CMOWebApi.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblUserMaster
    {
        public string UserType { get; set; }
        public string UserName { get; set; }
        public string UserPassword { get; set; }
        public System.DateTime UserCreateDate { get; set; }
        public Nullable<int> User_DepartmentCode { get; set; }
        public Nullable<int> User_DistrictCode { get; set; }
        public bool UserIsActive { get; set; }
        public bool UserIsDeleted { get; set; }
        public Nullable<int> User_SWUserCategotyCode { get; set; }
        public Nullable<int> User_ReferenceeCode { get; set; }
        public string SSOID { get; set; }
        public Nullable<int> User_GroupCode { get; set; }
        public Nullable<int> User_DepartmentGroupCode { get; set; }
        public int UserId { get; set; }
        public Nullable<int> OfficeCode { get; set; }
        public Nullable<decimal> User_DepartmentCode_CMS { get; set; }
        public Nullable<int> DistOfficerCode { get; set; }
        public string UserEmail { get; set; }
        public Nullable<bool> isUpdateProfileFirstTime { get; set; }
        public string NodalOfficerName { get; set; }
        public string Mobile { get; set; }
        public string Title { get; set; }
        public string Gender { get; set; }
        public string IPNo { get; set; }
        public string LandlineNo { get; set; }
        public string ProfilePic { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<int> DesignationCode { get; set; }
        public string Nickname { get; set; }
    }
}
