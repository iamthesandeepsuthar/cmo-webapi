﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CMOWebApi.Core.ExtensionMethods
{
    public static class CommonExtension
    {
        public static string ToThumbnailPath(this string filePath, string ThumbnailFolder = "Thumbnail", string ThumbnailPrifix = "Th_")
        {

            try
            {
                //string fileFullpath = HttpContext.Current.Server.MapPath(filePath);
                if (!string.IsNullOrEmpty(filePath))
                {
                    return filePath.Substring(0, filePath.LastIndexOf("/")) + "/" + (!string.IsNullOrEmpty(ThumbnailFolder) ? ThumbnailFolder + "/" : string.Empty) + (!string.IsNullOrEmpty(ThumbnailPrifix) ? ThumbnailPrifix : string.Empty) + filePath.Split('/')[filePath.Split('/').Length - 1];
                }
            }
            catch
            {
                return string.Empty;

            }
            return string.Empty;

        }

        /// <summary>
        /// get full  file name with base  path , You have to pass only folder path without server map path
        /// </summary>
        /// <param name="filePath">file path  without server.MapPath</param>
        /// <returns>domain.com/yourPath</returns>
        public static string ToAbsolutePath(this string filePath)

        {
            try
            {
                if (!string.IsNullOrEmpty(filePath))
                {
                    HttpRequest request = HttpContext.Current.Request;
                    return request.Url.AbsoluteUri.Replace(request.AppRelativeCurrentExecutionFilePath.Replace("~", "") + request.Url.Query, "/") + filePath.Replace("~", "");

                }
                return null;
            }
            catch (Exception)
            {
                return ConfigurationManager.AppSettings["BaseUrl"].ToString() + filePath.Replace("~", "");

            }

        }

        public static string ToPhysicalPath(this string filePath)
        {
            try
            {
                HttpRequest request = HttpContext.Current.Request;
                filePath = filePath.Replace(request.Url.AbsoluteUri.Replace(request.AppRelativeCurrentExecutionFilePath.Replace("~", "") + request.Url.Query, "/"), "~");
                return HttpContext.Current.Server.MapPath(filePath);

            }
            catch (Exception)
            {
                return HttpContext.Current.Server.MapPath(filePath);

            }
        }
        public static string ToHindiDate(this DateTime date, string format = null)
        {

            try
            {
                if (date.Month == 10)
                {
                 return   date.ToString(format, new System.Globalization.CultureInfo("hi-IN")).Replace("अक्तूबर", "अक्टूबर");
                }
                else
                {
                    return date.ToString(format, new System.Globalization.CultureInfo("hi-IN"));
                }





            }
            catch (Exception)
            {

                throw;
            }
        }


     

        public static bool CheckFileExist(this string filePath)
        {
            try
            {
                if (!string.IsNullOrEmpty(filePath) && File.Exists(HttpContext.Current.Server.MapPath(filePath)))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {


            }
            return false;
        }
    }



}
