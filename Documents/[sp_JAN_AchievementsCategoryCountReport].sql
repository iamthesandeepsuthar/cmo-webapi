
ALTER PROC [dbo].[sp_JAN_AchievementsCategoryCountReport] 
@DepartmentCategoryCode int=0
AS
BEGIN 

DECLARE @Qry NVARCHAR(MAX)=''
DECLARE @Qrys NVARCHAR(MAX)=''
SET @Qry=' SELECT ISNULL(DM.DepartmentCode,0) AS DepartmentCode, DM.DepartmentTitle'
SET @Qrys= ', (SELECT COUNT(Id) FROM tblADV_Achievements WHERE DepartmentCode=DM.DepartmentCode AND IsDeleted=0) AS Total'

CREATE TABLE #tempCategory
(
Id INT IDENTITY(1,1),
Code NVARCHAR(10),
Name NVARCHAR(250),
CONSTRAINT PK_tempTblAge PRIMARY KEY (Id)
)

INSERT INTO #tempCategory
SELECT CONVERT(NVARCHAR(10), CategoryCode), ReportName FROM tblADV_AchievementCategoryMaster WHERE IsActive=1 AND IsDeleted=0  and isnull(ReportName,'') <>'' ORDER BY ReportName

DECLARE @RowCount INT=0
SELECT @RowCount=COUNT(0) FROM #tempCategory

DECLARE @Index INT=1
WHILE @Index<=@RowCount
BEGIN
DECLARE @CategoryCode NVARCHAR(10) 
		DECLARE @CategoryName NVARCHAR(25) 
SELECT @CategoryCode=Code, @CategoryName=Name FROM #tempCategory WHERE Id=@Index

SET @Qry += ', (SELECT COUNT(Id) FROM tblADV_Achievements WHERE [AchievementCategoryCode]='+@CategoryCode+' AND DepartmentCode=DM.DepartmentCode AND IsDeleted=0) AS ['+@CategoryName+']'

SET @Index=@Index+1
END

SET @Qry += @Qrys+' FROM tblDepartmentMaster DM '
+ ' WHERE DM.DepartmentIsActive=1 AND DM.DepartmentIsDeleted=0 '
+ 'and isnull(DM.DepartmentCategoryCode,0)=' + case when @DepartmentCategoryCode>0 then CONVERT(varchar(100) , @DepartmentCategoryCode)     else 'isnull(DM.DepartmentCategoryCode,0)' end
+ ' ORDER BY DM.DepartmentTitle '

PRINT @Qry
EXEC (@Qry)

DROP TABLE #tempCategory

	
END 