﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CMOWebApi.Models.ComplaintModel
{
	public class CompliantEntryModel
	{
		public long Id { get; set; }
		[Required(ErrorMessage = "Description is required")]
		public string Description { get; set; }
		public Nullable<int> EntryTypeId { get; set; }
		public Nullable<int> PriorityId { get; set; }
		public Nullable<int> ModuleId { get; set; }
		public string ScreenURL { get; set; }
		public Nullable<int> StatusId { get; set; }
		public Nullable<bool> IsActive { get; set; } = true;
		public Nullable<bool> IsDelete { get; set; } = false;
		public Nullable<System.DateTime> CreatedDate { get; set; }
		public Nullable<int> CreatedBy { get; set; }
		public Nullable<System.DateTime> ModifiedDate { get; set; }
		public Nullable<int> ModifiedBy { get; set; }
		public List<ComplaintAttachmentModel> AttachmentList { get; set; }
        public string ApplicationCode { get; set; }
        public Nullable<long> PageCode { get; set; }
    }

	public class ComplaintAttachmentModel
	{
		public long Id { get; set; }
		public long ComplaintEntryId { get; set; }
		public string AttachmentsUrl { get; set; }
	}

	public class ComplaintEntryListModel
	{
		public long Id { get; set; }
		public string Description { get; set; }
		public Nullable<int> EntryTypeId { get; set; }
		public Nullable<int> PriorityId { get; set; }
		public string PriorityName { get; set; }
		public string EntryTypeName { get; set; }
		public string ModuleName { get; set; }
		public Nullable<int> ModuleId { get; set; }
		public string ScreenURL { get; set; }
		public Nullable<int> StatusId { get; set; }
		public string StatusName { get; set; }
		public Nullable<bool> IsActive { get; set; } 
		public Nullable<bool> IsDelete { get; set; }
		public Nullable<System.DateTime> CreatedDate { get; set; }
		public Nullable<int> CreatedBy { get; set; }
		public Nullable<System.DateTime> ModifiedDate { get; set; }
		public Nullable<int> ModifiedBy { get; set; }
        public string PageTitle { get; set; }
        public string ApplicationTitle { get; set; }
        public List<string> AttachmentList { get; set; }
	}

	public class CompliantActionModel
	{ 
		public long Id { get; set; }
		public long ComplaintEntryId { get; set; }
		public string AttachmentURL { get; set; }
		public string Comment { get; set; }
		public Nullable<int> StatusId { get; set; }
		public Nullable<bool> IsActive { get; set; } = true;
		public Nullable<bool> IsDeleted { get; set; } = false;
		public Nullable<System.DateTime> CreatedDate { get; set; }
		public Nullable<int> CreatedBy { get; set; }
		public Nullable<System.DateTime> ModifiedDate { get; set; }
		public Nullable<int> ModifiedBy { get; set; }

	}

}

