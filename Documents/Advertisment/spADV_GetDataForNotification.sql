
/***************************************************
Created By :Tanmaya 
Script Date : 16-09-2019
Description : To get data for notification(scheduler) 
				and insert non existing recorde 
				for admin department and department
exec [spADV_GetDataForNotification]
****************************************************/

 ALTER proc [dbo].[spADV_GetDataForNotification]
 as
 Begin
 ---Insert all department and admin department wich are not available at first time when set Isctive=true
 
 insert into [tblADV_NotificationLookup](AdvertisementCode,NotifiedUseDptCode,IsUploaded)
Select AdvertisementCode,NotifiedUseDptCode,0 as IsUploaded

from(

Select NotifiedUseDptCode,AdvertisementCode,

isnull(( select  count(*) from [tblADV_NotificationLookup]  where AdvertisementCode<>T.AdvertisementCode

and NotifiedUseDptCode<>T.AdvertisementCode),0) as _Count
 from (

select nm.Code as NotifiedUseDptCode,am.Code as AdvertisementCode
 from [tblADV_AdvertisementMaster] as am 
 inner join tblADV_NotificationMaster as nm on nm.MappingCode 
 in(select AdminDepartmentCode from tblADV_AdminDepartmentLookup where AdvertisementId=am.id) and am.isActive=1 and am.IsDeleted=0 and Type=1

 
 UNION
   -- Department User List from tblADV_NotificationMaster
 select nm.Code as NotifiedUseDptCode,am.Code as AdvertisementCode
 from [tblADV_AdvertisementMaster] as am 
 inner join tblADV_NotificationMaster as nm on nm.MappingCode 
 in(select DepartmentCode from tblDepartmentMaster where Department_AdmDepartmentCode in 
 ( select AdminDepartmentCode from tblADV_AdminDepartmentLookup where AdvertisementId=am.Id)) and am.IsActive=1 and IsDeleted=0 and Type=2
 
 ) as T
 )T1 where _Count<=0

 
--Send Notification about these user to admin---
 select am.Id as AdvId ,nm.NotificationPeriod, 'UsersForAdmin' as Type,
 nm.Email as UserEmail ,nm.MobileNo as UserMobileNo,nm.Id as NotificationMasterId

 from tblADV_AdvertisementMaster as am
 inner join tblADV_NotificationLookup as nl on am.Code=nl.AdvertisementCode
 inner join tblADV_NotificationMaster as nm on nm.Code=nl.NotifiedUseDptCode
 inner join  tblADV_RedesignPlatformUserLookup as rpu on rpu.NotificationLookupId=nl.Id

 where am.IsActive=1 and ISNULL(IsApproved,0)=0  and ISNULL(IsRejected,0)=0 and ExpiryDate>=getdate()
 --and ISNULL(LastNotifed,GETDATE())<=DATEADD(HOUR, -nm.NotificationPeriod, GETDATE())
  and ISNULL(LastNotifed,DATEADD(HOUR, -nm.NotificationPeriod, GETDATE()))<=DATEADD(HOUR, -nm.NotificationPeriod, GETDATE())
 and (select count(*) from tblADV_RedesignRequestByPlatformUserLookup as rpul where rpul.RedesignPlatformUserLookupId=ISNULL(rpu.Id,0) )>0

 UNION
 -- for non Approve Platform User, Admin Department and department ---
 select am.Id as AdvId ,nm.NotificationPeriod, 'UserDept' as Type,
 nm.Email as UserEmail ,nm.MobileNo as UserMobileNo,nm.Id as NotificationMasterId

 from tblADV_AdvertisementMaster as am
 inner join tblADV_NotificationLookup as nl on am.Code=nl.AdvertisementCode
 inner join tblADV_NotificationMaster as nm on nm.Code=nl.NotifiedUseDptCode

 where am.IsActive=1  and ExpiryDate>=getdate() and IsUploaded=0 
 --and ISNULL(LastNotifed,GETDATE())<=DATEADD(HOUR, -nm.NotificationPeriod, GETDATE())
 and ISNULL(LastNotifed,DATEADD(HOUR, -nm.NotificationPeriod, GETDATE()))<=DATEADD(HOUR, -nm.NotificationPeriod, GETDATE())
 and ISNULL(nm.IsApprove,0)=0
 --and nm.Type<>3
 UNION
  -- for Approve Platform User, Admin Department and department ---
  select am.Id as AdvId ,nm.NotificationPeriod, 'UserDept' as Type,
 nm.Email as UserEmail ,nm.MobileNo as UserMobileNo,nm.Id as NotificationMasterId

 from tblADV_AdvertisementMaster as am
 inner join tblADV_NotificationLookup as nl on am.Code=nl.AdvertisementCode
 inner join tblADV_NotificationMaster as nm on nm.Code=nl.NotifiedUseDptCode
 inner join  tblADV_RedesignPlatformUserLookup as rpu on rpu.NotificationLookupId=nl.Id

 where am.IsActive=1 and ExpiryDate>=getdate() and IsUploaded=0  and ISNULL(nm.IsApprove,0)=1
 --and ISNULL(LastNotifed,GETDATE())<=DATEADD(HOUR, -nm.NotificationPeriod, GETDATE())
  and ISNULL(LastNotifed,DATEADD(HOUR, -nm.NotificationPeriod, GETDATE()))<=DATEADD(HOUR, -nm.NotificationPeriod, GETDATE())

 UNION

 --Admin Details for Notification--
 
select 0 as AdvId,NotificationPeriod ,'Admin' as Type,Email as UserEmail,MobileNo as UserMobileNo, Id as  NotificationMasterId
 from tblADV_ApprovalDetailMaster
where IsDelete=0 and IsActive=1
 and ISNULL(LastNotifed,DATEADD(HOUR, -NotificationPeriod, GETDATE()))<=DATEADD(HOUR, -NotificationPeriod, GETDATE())
 --and ISNULL(LastNotifed,GETDATE())<=DATEADD(HOUR, -NotificationPeriod, GETDATE())


end


