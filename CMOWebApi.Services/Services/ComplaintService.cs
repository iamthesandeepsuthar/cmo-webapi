﻿using AutoMapper;
using CMOWebApi.Core;
using CMOWebApi.Data;
using CMOWebApi.Data.UnitOfWork;
using CMOWebApi.Models.ComplaintModel;
using CMOWebApi.Models.GeneralModel;
using CMOWebApi.Services.IServices;
using CMOWebApi.Services.ServiceHelper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static CMOWebApi.Core.Enums.ComplaintEnum;
using static CMOWebApi.Core.Enums.UserEnum;

namespace CMOWebApi.Services.Services
{
	public class ComplaintService : BaseService,IComplaintService
	{
		#region /// Variable ///
		IUnitofWork _uow;
		#endregion

		#region  /// Constructor ///
		public ComplaintService(IUnitofWork uow)
		{
			_uow = uow;
		}
		#endregion

		#region /// Method ///

		public ServiceResponse<PagedData<ComplaintEntryListModel>> GetAll(IndexModel model)

		{
			ServiceResponse<PagedData<ComplaintEntryListModel>> objReturn = new ServiceResponse<PagedData<ComplaintEntryListModel>>();
			try
			{

				PagedData<ComplaintEntryListModel> resulData = new PagedData<ComplaintEntryListModel>();
				PagedData<vw_COMPS_ComplaintEntry> data = new PagedData<vw_COMPS_ComplaintEntry>();

				string statusIds=model.AdvanceSearchModel != null && model.AdvanceSearchModel.Count > 0 ? (model.AdvanceSearchModel.ContainsKey("StatusIds") ? (model.AdvanceSearchModel["StatusIds"].ToString()) : string.Empty) : string.Empty;

				List<long?> ids = JsonConvert.DeserializeObject<List<long?>>(statusIds);

				if (_loginUserDetail.SSOID.ToLower() == "CMISNEWTEST1".ToLower())
				{
					 data = GenericGridCall<vw_COMPS_ComplaintEntry>.ListView(model.PageSize, x => x.CreatedDate, x => x.IsDelete == false && (ids != null && ids.Count>0?ids.Contains(x.StatusId): true) && x.IsDevFilter==true , model.Search, model.OrderBy, model.OrderByAsc, model.Page);
				}
               
                else if (_loginUserDetail.UserType.ToLower() == UserTypeEnum.ADM.ToString().ToLower() || _loginUserDetail.UserType.ToLower() == UserTypeEnum.SADM.ToString().ToLower())
				{
					 data = GenericGridCall<vw_COMPS_ComplaintEntry>.ListView(model.PageSize, x => x.CreatedDate, x => x.IsDelete == false && (ids != null && ids.Count > 0 ? ids.Contains(x.StatusId) : true) && x.IsAdmFilter == true, model.Search, model.OrderBy, model.OrderByAsc, model.Page);
				}
				else if (_loginUserDetail.UserType.ToLower() != UserTypeEnum.ADM.ToString().ToLower() || _loginUserDetail.UserType.ToLower() != UserTypeEnum.SADM.ToString().ToLower())
				{
				    data = GenericGridCall<vw_COMPS_ComplaintEntry>.ListView(model.PageSize, x => x.CreatedDate, x => x.IsDelete == false && (ids != null && ids.Count > 0 ? ids.Contains(x.StatusId) : true) && x.IsUserFilter == true, model.Search, model.OrderBy, model.OrderByAsc, model.Page);
				}

				var config = new MapperConfiguration(cfg =>
				{
                    cfg.CreateMap<vw_COMPS_ComplaintEntry, ComplaintEntryListModel>()
                     .ForMember(des => des.AttachmentList, src => src.MapFrom(x => !string.IsNullOrEmpty(x.Attachments) ? x.Attachments.Split(',') : null));
				});
				IMapper mapper = config.CreateMapper();
				resulData.Data = mapper.Map(data.Data, resulData.Data);
				PagedData<ComplaintEntryListModel>.ReturnCustomizeData(resulData, model.PageSize, data.TotalRecords);

				objReturn = SetResultStatus(resulData, MessageStatus.Success, true);
			}
			catch (Exception ex)
			{
				objReturn = SetResultStatus<PagedData<ComplaintEntryListModel>>(null, MessageStatus.Error, false);
			}
			return objReturn;
		}

		/// <summary>
		/// Create Compliant by user 
		/// </summary>
		/// <param name="model">model</param>
		/// <returns></returns>
		public async Task<ServiceResponse<string>> Create(CompliantEntryModel model)
		{
			try
			{
				Mapper.Initialize(x =>
				{
					x.CreateMap<CompliantEntryModel, tblCOMPS_ComplaintEntry>();
				});
				tblCOMPS_ComplaintEntry obj = Mapper.Map<CompliantEntryModel, tblCOMPS_ComplaintEntry>(model);
				obj.CreatedDate = DateTime.Now;
				obj.CreatedBy = _loginUserDetail.UserId;
				obj.StatusId = Convert.ToInt32(StatusEnum.New);
				_uow.GenericRepository<tblCOMPS_ComplaintEntry>().Add(obj);
				_uow.save();

				var ModuleName = _uow.GenericRepository<tblApplicationMaster>().GetAll(filter: x => x.ApplicationCode == model.ApplicationCode).OrderBy(x => x.ApplicationTitle).FirstOrDefault();
				if (model.AttachmentList != null && model.AttachmentList.Count > 0)
				{

					foreach (var item in model.AttachmentList)
					{
						if (!string.IsNullOrEmpty(item.AttachmentsUrl) || !string.IsNullOrEmpty(item.AttachmentsUrl))
						{
							tblCOMPS_AttachmentsLookUp objchild = new tblCOMPS_AttachmentsLookUp();

							var isValid = CommonUtility.IsAllowedMimeType(item.AttachmentsUrl, false, _loginUserDetail.FileSize);
							if (isValid.IsSuccess)
							{

								item.AttachmentsUrl = CommonUtility.UploadComplaint(item.AttachmentsUrl, ModuleName.ApplicationTitle.Replace(" ",""), string.Empty );
							}
							else if (CommonUtility.IsAllowedMimeType(item.AttachmentsUrl, true, _loginUserDetail.FileSize).IsSuccess)
							{
								item.AttachmentsUrl = CommonUtility.UploadComplaint(item.AttachmentsUrl, ModuleName.ApplicationTitle.Replace(" ", ""), string.Empty);
							}
							else
							{
								return isValid;
							}
							objchild.ComplaintEntryId = obj.Id;
							objchild.Path = item.AttachmentsUrl;
							await _uow.GenericRepository<tblCOMPS_AttachmentsLookUp>().AddAsync(objchild);
						}
					}
				}

				_uow.save();

				return SetResultStatus(string.Empty, MessageStatus.Create, true);
			}
			catch (Exception ex)
			{
				return SetResultStatus(string.Empty, MessageStatus.Error, false);
			}
		}

		/// <summary>
		/// take action by user, developer and administrator for compliant.
		/// </summary>
		/// <param name="model">model</param>
		/// <returns></returns>
		public async Task<ServiceResponse<string>> CreateAction(CompliantActionModel model)
		{
			try
			{ 

				var compliantObj = _uow.GenericRepository<tblCOMPS_ComplaintEntry>().GetByID(model.ComplaintEntryId);
				if (!string.IsNullOrEmpty(model.AttachmentURL ))
				{
					var isValid = CommonUtility.IsAllowedMimeType(model.AttachmentURL, false, _loginUserDetail.FileSize);
					if (isValid.IsSuccess)
					{
						model.AttachmentURL = CommonUtility.UploadAction(model.AttachmentURL, compliantObj.tblCOMPS_ModuleMaster.Name, string.Empty);

					}
				}
			
				Mapper.Initialize(x =>
				{
					x.CreateMap<CompliantActionModel, tblCOMPS_ActionLookUp>();
				});
				tblCOMPS_ActionLookUp obj = Mapper.Map<CompliantActionModel, tblCOMPS_ActionLookUp>(model);
				obj.CreatedDate = DateTime.Now;
				obj.CreatedBy = _loginUserDetail.UserId;
				_uow.GenericRepository<tblCOMPS_ActionLookUp>().Add(obj);

				 compliantObj.StatusId= obj.StatusId;
				await _uow.GenericRepository<tblCOMPS_ComplaintEntry>().UpdateAsync(compliantObj);
				_uow.save();
				return SetResultStatus(string.Empty, MessageStatus.Create, true);
			}
			catch (Exception ex)
			{
				return SetResultStatus(string.Empty, MessageStatus.Error, false);
			}
		}


		#endregion


	}
}
