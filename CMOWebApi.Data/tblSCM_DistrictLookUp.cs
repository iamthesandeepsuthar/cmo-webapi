//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CMOWebApi.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblSCM_DistrictLookUp
    {
        public long Id { get; set; }
        public long SchemeId { get; set; }
        public Nullable<long> DistrictCode { get; set; }
    
        public virtual tblSCM_SchemeMaster tblSCM_SchemeMaster { get; set; }
    }
}
