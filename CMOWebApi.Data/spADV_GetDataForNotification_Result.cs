//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CMOWebApi.Data
{
    using System;
    
    public partial class spADV_GetDataForNotification_Result
    {
        public long AdvId { get; set; }
        public Nullable<decimal> NotificationPeriod { get; set; }
        public string Type { get; set; }
        public string UserEmail { get; set; }
        public string UserMobileNo { get; set; }
        public long NotificationMasterId { get; set; }
    }
}
