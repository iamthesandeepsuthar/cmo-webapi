//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CMOWebApi.Data
{
    using System;
    
    public partial class sp_ConfigurationSCHEME_Result
    {
        public Nullable<long> SchemeId { get; set; }
        public string SchemeName { get; set; }
        public Nullable<long> DepartmentCode { get; set; }
        public Nullable<long> DisplayOrder { get; set; }
        public Nullable<int> PermissionId { get; set; }
        public string Type { get; set; }
    }
}
