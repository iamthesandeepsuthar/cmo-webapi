using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMOWebApi.Models.AutoGeneratedModels
{
    [Table("tblState")]
    public partial class tblState
    {
        [Key]
        public int Id { get; set; }
        public string StateName { get; set; }
        public int CountryID { get; set; }
        public bool IsActive { get; set; }
        [ForeignKey("CountryID")]
        public virtual tblCountry tblCountry { get; set; }
    }
}
