﻿//using CMOWebApi.Models.LMSModel;
//using CMOWebApi.Services.ServiceHelper;
//using System.Collections.Generic;

//namespace CMOWebApi.Services.IServices
//{
//	public interface IDashboardService
//    {
//        /// <summary>
//        /// Get Department Notification Count Report
//        /// </summary>
//        /// <param name="userId"></param>
//        /// <returns>Method returns DepartmentNotificaionCountViewModel list</returns>
//        /// <developer>Harry</developer>
//        ServiceResponse<List<DepartmentNotificaionCountViewModel>> GetDepartmentNotificationCountReport(int userId);

//        /// <summary>
//        /// Get Department Action Status Count Report
//        /// </summary>
//        /// <param name="userId"></param>
//        /// <returns>Method returns DepartmentActionStatusCountViewModel list</returns>
//        /// <developer>Harry</developer>
//        ServiceResponse<List<DepartmentActionStatusCountViewModel>> GetDepartmentActionStatusCountReport(int userId);

//        /// <summary>
//        /// Get Department Action Age Wise Count Report
//        /// </summary>
//        /// <param name="userId"></param>
//        /// <returns>Method returns DepartmentActionAgeWiseCountViewModel list</returns>
//        /// <developer>Harry</developer>
//        ServiceResponse<List<DepartmentActionAgeWiseCountViewModel>> GetDepartmentActionAgeWiseCountReport(int userId);

//        /// <summary>
//        /// Get Department Last 6 Month Action Count Report
//        /// </summary>
//        /// <param name="userId"></param>
//        /// <returns>Method returns DepartmentLast6MonthActionCountViewModel list</returns>
//        /// <developer>Harry</developer>
//        ServiceResponse<List<DepartmentLast6MonthActionCountViewModel>> GetDepartmentLast6MonthActionCountReport(int userId);

//        /// <summary>
//        /// Get Department Dashboard Report
//        /// </summary>
//        /// <param name="userId"></param>
//        /// <returns>Method returns DepartmentDashboardViewModel</returns>
//        /// <developer>Harry</developer>
//        ServiceResponse<DepartmentDashboardViewModel> GetDepartmentDashboardReportData(int userId);

//    }
//}
