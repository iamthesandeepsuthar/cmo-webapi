
/***************************************************
Created By :Tanmaya 
Script Date : 14-10-2019
Description : Get Monitoring pArameter Data by 
				SchemeId
exec [spSCM_MonitoringParameterData] 5
****************************************************/

 alter proc [dbo].[spSCM_MonitoringParameterData]
  @SchemeId bigint=0
 as
 Begin
 select MappingTableName as FieldName,pde.FieldId as MonitoringParamId ,FieldValue,DataEntryId,
dm.DepartmentTitle as FieldDisplayValue
from [tblSCM_MonitoringParameterDataEntryFieldValue] as pde
inner join tblSCM_MonitoringParameterMaster as pm on pde.FieldId=pm.Id
left join tblDepartmentMaster as dm on pde.FieldValue= dm.DepartmentCode
where pde.SchemeId=@SchemeId and pm.Type='DDL' and pm.MappingTableName='tblDepartmentMaster'

union

 select MappingTableName as FieldName,pde.FieldId as MonitoringParamId ,FieldValue,DataEntryId,
bc.ansmtcategory as FieldDisplayValue
from [tblSCM_MonitoringParameterDataEntryFieldValue] as pde
inner join tblSCM_MonitoringParameterMaster as pm on pde.FieldId=pm.Id
left join tblBeneficiaryCagegory as bc on  pde.FieldValue=  bc.cm_ansmtcategoryid
where pde.SchemeId=@SchemeId and pm.Type='DDL' and pm.MappingTableName='tblBeneficiaryCagegory'

union

 select MappingTableName as FieldName,pde.FieldId as MonitoringParamId ,FieldValue,DataEntryId,
cm.Category as FieldDisplayValue
from [tblSCM_MonitoringParameterDataEntryFieldValue] as pde
inner join tblSCM_MonitoringParameterMaster as pm on pde.FieldId=pm.Id
left join tblCategoryMaster as cm on  pde.FieldValue=  cm.CategoryCode
where pde.SchemeId=@SchemeId and pm.Type='DDL' and pm.MappingTableName='tblCategoryMaster'

union

 select MappingTableName as FieldName,pde.FieldId as MonitoringParamId ,FieldValue,DataEntryId,
FieldValue as FieldDisplayValue
from [tblSCM_MonitoringParameterDataEntryFieldValue] as pde
inner join tblSCM_MonitoringParameterMaster as pm on pde.FieldId=pm.Id
where pde.SchemeId=@SchemeId and pm.Type<>'DDL' 

end


