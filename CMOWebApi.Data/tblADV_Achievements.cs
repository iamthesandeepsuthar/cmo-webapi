//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CMOWebApi.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblADV_Achievements
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblADV_Achievements()
        {
            this.tblAchievement_ConnectWithCMIS_Parameter = new HashSet<tblAchievement_ConnectWithCMIS_Parameter>();
            this.tblADV_AchievementAttachments = new HashSet<tblADV_AchievementAttachments>();
            this.tblADV_AchievementBenificiaryMapping = new HashSet<tblADV_AchievementBenificiaryMapping>();
        }
    
        public int Id { get; set; }
        public string Achievement { get; set; }
        public string AchievementHindi { get; set; }
        public Nullable<int> AchievementCategoryCode { get; set; }
        public Nullable<int> DepartmentCode { get; set; }
        public string Description { get; set; }
        public string DescriptionHindi { get; set; }
        public int Priority { get; set; }
        public string PdfFIleName { get; set; }
        public string Url { get; set; }
        public Nullable<System.DateTime> AchievementDate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsVisible { get; set; }
        public Nullable<long> AchievementSubCategoryCode { get; set; }
        public string CMOComments { get; set; }
        public string KeyWord { get; set; }
        public string AutoKeyWord { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblAchievement_ConnectWithCMIS_Parameter> tblAchievement_ConnectWithCMIS_Parameter { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblADV_AchievementAttachments> tblADV_AchievementAttachments { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblADV_AchievementBenificiaryMapping> tblADV_AchievementBenificiaryMapping { get; set; }
        public virtual tblADV_AchievementCategoryMaster tblADV_AchievementCategoryMaster { get; set; }
    }
}
