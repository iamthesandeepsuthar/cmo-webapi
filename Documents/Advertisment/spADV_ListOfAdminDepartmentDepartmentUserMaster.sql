
/****************************************
Created By :Tanmaya 
Script Date : 09-09-2019
Description : To get Admin department, Department and 
				 platform user for sending notification 
					when make IsActive=true
Code =3 -- Plateform user
Code =2 -- Department
Type =1 -- Admin Department
exec [spADV_ListOfAdminDepartmentDepartmentUserMaster] 2
*********************************** ******/

 ALTER proc  [dbo].[spADV_ListOfAdminDepartmentDepartmentUserMaster]

 @AdvId bigint=0

as

Begin
declare @advCode bigint =0

 set @advCode=(select code from tblADV_AdvertisementMaster where Id=@AdvId)

  --Admin Department User List from tblADV_NotificationMaster
 select Code as UserCode, 'AdminDepartment' as Type,'' as Name,Email,MobileNo ,isnull(IsApprove,0) as IsApprove,Code as NotifiedUseCode,@advCode as AdvertisementCode
 from tblADV_NotificationMaster where MappingCode 
 in(select AdminDepartmentCode from tblADV_AdminDepartmentLookup where AdvertisementId=@AdvId) and isActive=1 and IsDelete=0 and Type=1

 
 UNION
   -- Department User List from tblADV_NotificationMaster
 select Code as UserCode,'Department' as Type ,'' as Name,Email,MobileNo ,isnull(IsApprove,0) as IsApprove,Code as NotifiedUseCode,@advCode as AdvertisementCode
 from tblADV_NotificationMaster where MappingCode 
 in(select DepartmentCode from tblDepartmentMaster where Department_AdmDepartmentCode in 
 ( select AdminDepartmentCode from tblADV_AdminDepartmentLookup where AdvertisementId=@AdvId)) and isActive=1 and IsDelete=0 and Type=2

 UNION

  --Platform user list from tblADV_NotificationMaster
 select Code as UserCode, 'PlatformUser' as Type ,'' as Name,Email,MobileNo ,isnull(IsApprove,0) as IsApprove,Code as NotifiedUseCode,@advCode as AdvertisementCode
 from  tblADV_NotificationMaster as nm where Type=3 and isActive=1 and IsDelete=0

 UNION

  --Admin Department User List from tblADV_AdminDepartmentLookup
  select AdminDepartmentCode as UserCode,'AdminDepartment' as Type ,adm.AdmDepartmentTitle as Name,'' as Email,'' as MobileNo  
  ,'' as IsApprove ,'' as NotifiedUseCode,@advCode as AdvertisementCode
  from tblADV_AdminDepartmentLookup as dl 
  inner join tblAdmDepartmentMaster as adm on adm.AdmDepartmentCode=dl.AdminDepartmentCode
  where AdvertisementId=@AdvId and AdminDepartmentCode not in (select Code from tblADV_NotificationMaster where MappingCode 
 in(select AdminDepartmentCode from tblADV_AdminDepartmentLookup where AdvertisementId=@AdvId) and isActive=1 and IsDelete=0 and Type=1)

 UNION

    -- Department User List from tblDepartmentMaster
select DepartmentCode as UserCode, 'Department' as Type,DepartmentTitle as Name,'' as Email,'' as MobileNo ,'' as IsApprove
,'' as NotifiedUseCode,@advCode as AdvertisementCode
from tblDepartmentMaster
 where Department_AdmDepartmentCode in 
 ( select AdminDepartmentCode from tblADV_AdminDepartmentLookup where AdvertisementId=@AdvId) 
 and DepartmentCode not in( select Code  from tblADV_NotificationMaster where MappingCode 
 in(select DepartmentCode from tblDepartmentMaster where Department_AdmDepartmentCode in 
 ( select AdminDepartmentCode from tblADV_AdminDepartmentLookup where AdvertisementId=@AdvId)) and isActive=1 and IsDelete=0 and Type=2)


 ---[tblADV_ApprovalDetailMaster]
 --select * from [dbo].[tblADV_ApprovalDetailMaster]

 --tblADV_AdvertisementMaster
  --select * from tblADV_AdvertisementMaster where Id=@AdvId

end

