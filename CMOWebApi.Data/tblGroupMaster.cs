//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CMOWebApi.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblGroupMaster
    {
        public int GroupId { get; set; }
        public Nullable<int> GroupCode_Old { get; set; }
        public string GroupTitle { get; set; }
        public string GroupTitleHindi { get; set; }
        public string GroupOfficerName { get; set; }
        public string GroupOfficerNameHindi { get; set; }
        public string GroupDesgDescription { get; set; }
        public string GroupDesgDescriptionHindi { get; set; }
        public string GroupGender { get; set; }
        public Nullable<int> Group_InHouseDesignationCode_old { get; set; }
        public Nullable<int> Group_SecondLevelCode { get; set; }
        public bool GroupIsActive { get; set; }
        public bool GroupIsDeleted { get; set; }
        public Nullable<int> GroupCode { get; set; }
        public Nullable<int> Group_InHouseDesignationCode { get; set; }
        public Nullable<int> GroupIsOfficerforNMS { get; set; }
        public string GroupEmail { get; set; }
        public string GroupMobile { get; set; }
    }
}
