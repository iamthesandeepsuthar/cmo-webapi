﻿using AutoMapper;
using CMOWebApi.Core;
using CMOWebApi.Data;
using CMOWebApi.Data.UnitOfWork;
using CMOWebApi.Models.AdminModel.MasterModel;
using CMOWebApi.Services.IServices;
using CMOWebApi.Services.ServiceHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace CMOWebApi.Services.Services
{
    public class JankalyanReportService : BaseService, IJankalyanReportService
    {
        #region /// Variable ///
        IUnitofWork _uow;

        #endregion

        #region /// constructor  ///
        public JankalyanReportService(IUnitofWork uow)
        {
            _uow = uow;
        }
        #endregion

        #region Jankalyan Front Report

        /// <summary>
        /// Get summary report of jankalyan front portal of user visit from back-end
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ServiceResponse<List<JankalyanUserLogSummaryReportModel>> GetJankalyanUserLogSummaryReport(JankalyanLogSearchModel model)
        {
            try
            {
                List<JankalyanUserLogSummaryReportModel> objReturn = new List<JankalyanUserLogSummaryReportModel>();
                List<SP_JAN_JankalyanUserLogSummaryReport_Result> data = _uow.ExeccuteStoreProcedure<SP_JAN_JankalyanUserLogSummaryReport_Result>("SP_JAN_JankalyanUserLogSummaryReport @DepartmentCode,@OfficeCode,@District,@UserType, @ToDate, @FromDate, @CustomSearch"
                  , new SqlParameter("DepartmentCode", SqlDbType.Int) { Value = model.DepartmentCode > 0 ? model.DepartmentCode : 0 }
                  , new SqlParameter("OfficeCode", SqlDbType.Int) { Value = model.OfficeCode > 0 ? model.OfficeCode : 0 }
                  , new SqlParameter("District", SqlDbType.Int) { Value = model.District > 0 ? model.District : 0 }
                  , new SqlParameter("UserType", SqlDbType.NVarChar) { Value = string.IsNullOrEmpty(model.UserType) ? string.Empty : model.UserType }
                  , new SqlParameter("ToDate", SqlDbType.NVarChar) { Value = string.IsNullOrEmpty(model.ToDate) ? string.Empty : model.ToDate }
                  , new SqlParameter("FromDate", SqlDbType.NVarChar) { Value = string.IsNullOrEmpty(model.FromDate) ? string.Empty : model.FromDate }
                  , new SqlParameter("CustomSearch", SqlDbType.NVarChar) { Value = string.IsNullOrEmpty(model.CustomSearch) ? string.Empty : model.CustomSearch }).ToList();

                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<SP_JAN_JankalyanUserLogSummaryReport_Result, JankalyanUserLogSummaryReportModel>();
                });
                IMapper mapper = config.CreateMapper();
                objReturn = mapper.Map(data, objReturn);

                return SetResultStatus(objReturn, MessageStatus.Success, true);
            }
            catch (Exception ex)
            {
                CreateLogHelper.CreateLogFile("GetJankalyanUserLogSummaryReport ex.Message :" + ex.Message + " \n");
                CreateLogHelper.CreateLogFile("GetJankalyanUserLogSummaryReport ex.InnerException.Message :" + ex.InnerException.Message + " \n");
                CreateLogHelper.CreateLogFile("GetJankalyanUserLogSummaryReport ex.InnerException.InnerException.Message :" + ex.InnerException.InnerException.Message + " \n");
                return SetResultStatus<List<JankalyanUserLogSummaryReportModel>>(null, MessageStatus.Error, false);
            }
        }

        /// <summary>
        /// Get Detail report of jankalyan front portal of user visit from back-end
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ServiceResponse<List<JankalyanUserLogDetailReportModel>> GetJankalyanUserLogDetailReport(JankalyanLogSearchModel model)
        {
            try
            {
                List<JankalyanUserLogDetailReportModel> objReturn = new List<JankalyanUserLogDetailReportModel>();
                List<SP_JAN_JankalyanUserLogDetailReport_Result> data = _uow.ExeccuteStoreProcedure<SP_JAN_JankalyanUserLogDetailReport_Result>("SP_JAN_JankalyanUserLogDetailReport @DepartmentCode,@OfficeCode,@District,@UserType, @ToDate, @FromDate, @CustomSearch"
                  , new SqlParameter("DepartmentCode", SqlDbType.Int) { Value = model.DepartmentCode > 0 ? model.DepartmentCode : 0 }
                  , new SqlParameter("OfficeCode", SqlDbType.Int) { Value = model.OfficeCode > 0 ? model.OfficeCode : 0 }
                  , new SqlParameter("District", SqlDbType.Int) { Value = model.District > 0 ? model.District : 0 }
                  , new SqlParameter("UserType", SqlDbType.NVarChar) { Value = string.IsNullOrEmpty(model.UserType) ? string.Empty : model.UserType }
                  , new SqlParameter("ToDate", SqlDbType.NVarChar) { Value = string.IsNullOrEmpty(model.ToDate) ? string.Empty : model.ToDate }
                  , new SqlParameter("FromDate", SqlDbType.NVarChar) { Value = string.IsNullOrEmpty(model.FromDate) ? string.Empty : model.FromDate }
                  , new SqlParameter("CustomSearch", SqlDbType.NVarChar) { Value = string.IsNullOrEmpty(model.CustomSearch) ? string.Empty : model.CustomSearch }).ToList();

                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<SP_JAN_JankalyanUserLogDetailReport_Result, JankalyanUserLogDetailReportModel>();
                });
                IMapper mapper = config.CreateMapper();
                objReturn = mapper.Map(data, objReturn);

                return SetResultStatus(objReturn, MessageStatus.Success, true);
            }
            catch (Exception ex)
            {
                CreateLogHelper.CreateLogFile("GetJankalyanUserLogDetailReport ex.Message :" + ex.Message + " \n");
                CreateLogHelper.CreateLogFile("GetJankalyanUserLogDetailReport ex.InnerException.Message :" + ex.InnerException.Message + " \n");
                CreateLogHelper.CreateLogFile("GetJankalyanUserLogDetailReport ex.InnerException.InnerException.Message :" + ex.InnerException.InnerException.Message + " \n");
                return SetResultStatus<List<JankalyanUserLogDetailReportModel>>(null, MessageStatus.Error, false);
            }
        }

        /// <summary>
        /// Get summary report of jankalyan portal 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ServiceResponse<List<JankalyanSummaryReportModel>> GetJankalyanSummaryReport(JankalyanSummarySearchModel model)
        {
            try
            {
                List<JankalyanSummaryReportModel> objReturn = new List<JankalyanSummaryReportModel>();
                List<sp_JAN_JankalyanSummaryReport_Result> data = _uow.ExeccuteStoreProcedure<sp_JAN_JankalyanSummaryReport_Result>("sp_JAN_JankalyanSummaryReport @DepartmentCode, @AdminDepartmentCode,@Status, @EntryFromDate, @EntryToDate"
                  , new SqlParameter("DepartmentCode", SqlDbType.Int) { Value = model.DepartmentCode > 0 ? model.DepartmentCode : 0 }
                   , new SqlParameter("AdminDepartmentCode", SqlDbType.Int) { Value = model.AdminDepartmentCode > 0 ? model.AdminDepartmentCode : 0 }
                  , new SqlParameter("Status", SqlDbType.Int) { Value = model.Status }
                   , new SqlParameter("EntryFromDate", SqlDbType.NVarChar) { Value = string.IsNullOrEmpty(model.EntryFromDate) ? string.Empty : model.EntryFromDate }
                      , new SqlParameter("EntryToDate", SqlDbType.NVarChar) { Value = string.IsNullOrEmpty(model.EntryToDate) ? string.Empty : model.EntryToDate }
                  ).ToList();

                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<sp_JAN_JankalyanSummaryReport_Result, JankalyanSummaryReportModel>();
                });
                IMapper mapper = config.CreateMapper();
                objReturn = mapper.Map(data, objReturn);

                return SetResultStatus(objReturn, MessageStatus.Success, true);
            }
            catch (Exception ex)
            {
                CreateLogHelper.CreateLogFile("GetJankalyanSummaryReport ex.Message :" + ex.Message + " \n");
                CreateLogHelper.CreateLogFile("GetJankalyanSummaryReport ex.InnerException.Message :" + ex.InnerException.Message + " \n");
                CreateLogHelper.CreateLogFile("GetJankalyanSummaryReport ex.InnerException.InnerException.Message :" + ex.InnerException.InnerException.Message + " \n");
                return SetResultStatus<List<JankalyanSummaryReportModel>>(null, MessageStatus.Error, false);
            }
        }

        #endregion

        #region CMIS New Report

        /// <summary>
        /// Get summary report CMIS new module which used on jankalyan portal 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ServiceResponse<List<CMISNewSummaryModel>> GetCMISNewSummaryReport(CMISReportFilterModel model)
        {
            try
            {
                List<CMISNewSummaryModel> objReturn = new List<CMISNewSummaryModel>();
                List<CMIS_New_Summary_Result> data = _uow.ExeccuteStoreProcedure<CMIS_New_Summary_Result>("CMIS_New_Summary @dept_Code,@DeptStatus, @CMOStatus, @ModuleID,@FromDate,@ToDate"
                  , new SqlParameter("dept_Code", SqlDbType.Decimal) { Value = model.DepartmentCode > 0 ? model.DepartmentCode : 0 }
                  , new SqlParameter("DeptStatus", SqlDbType.VarChar) { Value = !string.IsNullOrEmpty(model.DepartmentStatus) ? model.DepartmentStatus : string.Empty }
                  , new SqlParameter("CMOStatus", SqlDbType.VarChar) { Value = !string.IsNullOrEmpty(model.CMOStatus) ? model.CMOStatus : string.Empty }
                  , new SqlParameter("ModuleID", SqlDbType.Decimal) { Value = model.ModuleId > 0 ? model.ModuleId : 0 }
                  , new SqlParameter("FromDate", SqlDbType.NVarChar) { Value = string.IsNullOrEmpty(model.FromDate) ? string.Empty : model.FromDate }
                  , new SqlParameter("ToDate", SqlDbType.NVarChar) { Value = string.IsNullOrEmpty(model.ToDate) ? string.Empty : model.ToDate }
                  ).ToList();

                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<CMIS_New_Summary_Result, CMISNewSummaryModel>();
                });
                IMapper mapper = config.CreateMapper();
                objReturn = mapper.Map(data, objReturn);

                return SetResultStatus(objReturn, MessageStatus.Success, true);
            }
            catch (Exception ex)
            {
                CreateLogHelper.CreateLogFile("GetCMISNewSummaryReport ex.Message :" + ex.Message + " \n");
                CreateLogHelper.CreateLogFile("GetCMISNewSummaryReport ex.InnerException.Message :" + ex.InnerException.Message + " \n");
                CreateLogHelper.CreateLogFile("GetCMISNewSummaryReport ex.InnerException.InnerException.Message :" + ex.InnerException.InnerException.Message + " \n");
                return SetResultStatus<List<CMISNewSummaryModel>>(null, MessageStatus.Error, false);
            }
        }

        /// <summary>
        /// Get detail report CMIS new module which used on jankalyan portal 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ServiceResponse<List<CMISNewDetailModel>> GetCMISNewDetailReport(CMISReportFilterModel model)
        {
            try
            {
                List<CMISNewDetailModel> objReturn = new List<CMISNewDetailModel>();
                List<CMIS_New_detail_Result> data = _uow.ExeccuteStoreProcedure<CMIS_New_detail_Result>("CMIS_New_detail @dept_Code,@DeptStatus, @CMOStatus, @ModuleID,@FromDate,@ToDate"
                  , new SqlParameter("dept_Code", SqlDbType.Decimal) { Value = model.DepartmentCode > 0 ? model.DepartmentCode : 0 }
                 , new SqlParameter("DeptStatus", SqlDbType.VarChar) { Value = !string.IsNullOrEmpty(model.DepartmentStatus) ? model.DepartmentStatus : string.Empty }
                  , new SqlParameter("CMOStatus", SqlDbType.VarChar) { Value = !string.IsNullOrEmpty(model.CMOStatus) ? model.CMOStatus : string.Empty }
                  , new SqlParameter("ModuleID", SqlDbType.Decimal) { Value = model.ModuleId > 0 ? model.ModuleId : 0 }
                  , new SqlParameter("FromDate", SqlDbType.VarChar) { Value = string.IsNullOrEmpty(model.FromDate) ? string.Empty : model.FromDate }
                  , new SqlParameter("ToDate", SqlDbType.VarChar) { Value = string.IsNullOrEmpty(model.ToDate) ? string.Empty : model.ToDate }
                  ).ToList();

                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<CMIS_New_detail_Result, CMISNewDetailModel>();
                });
                IMapper mapper = config.CreateMapper();
                objReturn = mapper.Map(data, objReturn);

                return SetResultStatus(objReturn, MessageStatus.Success, true);
            }
            catch (Exception ex)
            {
                CreateLogHelper.CreateLogFile("GetCMISNewDetailReport ex.Message :" + ex.Message + " \n");
                CreateLogHelper.CreateLogFile("GetCMISNewDetailReport ex.InnerException.Message :" + ex.InnerException.Message + " \n");
                CreateLogHelper.CreateLogFile("GetCMISNewDetailReport ex.InnerException.InnerException.Message :" + ex.InnerException.InnerException.Message + " \n");
                return SetResultStatus<List<CMISNewDetailModel>>(null, MessageStatus.Error, false);
            }
        }

        #endregion

        #region CMIS Compliance

        /// <summary>
        /// get CMIS Compliance by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ServiceResponse<CMISComplianceModel> GetCMISComplianceById(int id,int achvId)
        {
            try
            {
                tblJAN_CMIS_Compliance sector = _uow.GenericRepository<tblJAN_CMIS_Compliance>().GetAll(filter: x => (id > 0? x.CMISNew_Trans_CoreId == id:false) || (achvId > 0 ? x.CMIS_AchievementId == achvId : false)).FirstOrDefault();

                CMISComplianceModel objData = new CMISComplianceModel();
                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<tblJAN_CMIS_Compliance, CMISComplianceModel>();
                });
                IMapper mapper = config.CreateMapper();
                objData = mapper.Map(sector, objData);

                return SetResultStatus(objData, MessageStatus.Success, true);
            }
            catch (Exception ex)
            {
                CreateLogHelper.CreateLogFile("GetCMISComplianceById ex.Message :" + ex.Message + " \n");
                CreateLogHelper.CreateLogFile("GetCMISComplianceById ex.InnerException.Message :" + ex.InnerException.Message + " \n");
                CreateLogHelper.CreateLogFile("GetCMISComplianceById ex.InnerException.InnerException.Message :" + ex.InnerException.InnerException.Message + " \n");
                return SetResultStatus<CMISComplianceModel>(null, MessageStatus.Error, false);
            }

        }

        /// <summary>
        /// Add and update CMIS Compliance and generate UIN numer at the time of Add new data
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ServiceResponse<string> AddUpdateCMISCompliance(CMISComplianceModel model)
        {
            try
            {
                spJAN_CMISComplianceSave_Result data = _uow.ExeccuteStoreProcedure<spJAN_CMISComplianceSave_Result>("spJAN_CMISComplianceSave @Id,@CMISNew_Trans_CoreId,@ModuleName,@CategoryCode,@SubCategoryCode,@WorksTypeCode,@PageCode,@LoginUserId,@CMIS_AchievementId",
                 new SqlParameter("Id", SqlDbType.BigInt) { Value = model.Id },
                 new SqlParameter("CMISNew_Trans_CoreId", SqlDbType.Int) { Value = model.CMISNew_Trans_CoreId >0?model.CMISNew_Trans_CoreId: (object)DBNull.Value },
                 new SqlParameter("ModuleName", SqlDbType.NVarChar) { Value = model.ModuleName },
                 new SqlParameter("CategoryCode", SqlDbType.Int) { Value = model.CategoryCode },
                 new SqlParameter("SubCategoryCode", SqlDbType.Int) { Value = model.SubCategoryCode },
                 new SqlParameter("WorksTypeCode", SqlDbType.Int) { Value = model.WorksTypeCode },
                 new SqlParameter("PageCode", SqlDbType.BigInt) { Value = model.PageCode },
                 new SqlParameter("LoginUserId", SqlDbType.Int) { Value = _loginUserDetail.UserId },
                  new SqlParameter("CMIS_AchievementId", SqlDbType.Int) { Value = model.CMIS_AchievementId>0?model.CMIS_AchievementId:0 }
                 ).FirstOrDefault();
                var id = data.Id;

                return SetResultStatus(id.ToString(), model.Id > 0 ? MessageStatus.Update : MessageStatus.Create, true);
            }
            catch (Exception ex)
            {
                CreateLogHelper.CreateLogFile("AddUpdateCMISCompliance ex.Message :" + ex.Message + " \n");
                CreateLogHelper.CreateLogFile("AddUpdateCMISCompliance ex.InnerException.Message :" + ex.InnerException.Message + " \n");
                CreateLogHelper.CreateLogFile("AddUpdateCMISCompliance ex.InnerException.InnerException.Message :" + ex.InnerException.InnerException.Message + " \n");
                return SetResultStatus<string>(null, MessageStatus.Error, false);
            }

        }

        #endregion

        #region Compliance Report

        /// <summary>
        /// Get CMIS Compliance report 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ServiceResponse<List<ComplianceReportModel>> GetCMISComplianceReport(CMISReportFilterModel model)
        {
            try
            {
                List<ComplianceReportModel> objReturn = new List<ComplianceReportModel>();
                List<SP_JAN_CMIS_ComplianceReport_Result> data = _uow.ExeccuteStoreProcedure<SP_JAN_CMIS_ComplianceReport_Result>("SP_JAN_CMIS_ComplianceReport @DepartmentCode,@ModuleID, @CMOStatus, @DepartmentStatus,@ToDate,@FromDate,@CMOOfficerCode"
                  , new SqlParameter("DepartmentCode", SqlDbType.Decimal) { Value = model.DepartmentCode > 0 ? model.DepartmentCode : 0 }
                  , new SqlParameter("ModuleID", SqlDbType.Decimal) { Value = model.ModuleId > 0 ? model.ModuleId : 0 }
                  , new SqlParameter("CMOStatus", SqlDbType.VarChar) { Value = !string.IsNullOrEmpty(model.CMOStatus) ? model.CMOStatus : string.Empty }
                  , new SqlParameter("DepartmentStatus", SqlDbType.VarChar) { Value = !string.IsNullOrEmpty(model.DepartmentStatus) ? model.DepartmentStatus : string.Empty }
                  , new SqlParameter("ToDate", SqlDbType.NVarChar) { Value = string.IsNullOrEmpty(model.ToDate) ? string.Empty : model.ToDate }
                  , new SqlParameter("FromDate", SqlDbType.NVarChar) { Value = string.IsNullOrEmpty(model.FromDate) ? string.Empty : model.FromDate }
                   , new SqlParameter("CMOOfficerCode", SqlDbType.BigInt) { Value = model.CMOOfficerCode > 0 ? model.CMOOfficerCode : 0 }
                  ).ToList();

                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<SP_JAN_CMIS_ComplianceReport_Result, ComplianceReportModel>();
                });
                IMapper mapper = config.CreateMapper();
                objReturn = mapper.Map(data, objReturn);

                return SetResultStatus(objReturn, MessageStatus.Success, true);
            }
            catch (Exception ex)
            {
                CreateLogHelper.CreateLogFile("GetCMISComplianceReport ex.Message :" + ex.Message + " \n");
                CreateLogHelper.CreateLogFile("GetCMISComplianceReport ex.InnerException.Message :" + ex.InnerException.Message + " \n");
                CreateLogHelper.CreateLogFile("GetCMISComplianceReport ex.InnerException.InnerException.Message :" + ex.InnerException.InnerException.Message + " \n");
                return SetResultStatus<List<ComplianceReportModel>>(null, MessageStatus.Error, false);
            }
        }

        /// <summary>
        /// Get CMIS Module and department wise Compliance summary report report 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ServiceResponse<List<ComplianceModuleAndDeptWiseSummaryReportModel>> GetComplianceModuleAndDeptWiseSummaryReport(CMISReportFilterModel model)
        {
            try
            {
                List<ComplianceModuleAndDeptWiseSummaryReportModel> objReturn = new List<ComplianceModuleAndDeptWiseSummaryReportModel>();
                List<SP_JAN_ComplianceModuleAndDeptWiseSummaryReport_Result> data = _uow.ExeccuteStoreProcedure<SP_JAN_ComplianceModuleAndDeptWiseSummaryReport_Result>("SP_JAN_ComplianceModuleAndDeptWiseSummaryReport @DepartmentCode,@ModuleID, @CMOStatus, @DepartmentStatus,@ToDate,@FromDate,@CMOOfficerCode"
                  , new SqlParameter("DepartmentCode", SqlDbType.Decimal) { Value = model.DepartmentCode > 0 ? model.DepartmentCode : 0 }
                  , new SqlParameter("ModuleID", SqlDbType.Decimal) { Value = model.ModuleId > 0 ? model.ModuleId : 0 }
                  , new SqlParameter("CMOStatus", SqlDbType.VarChar) { Value = !string.IsNullOrEmpty(model.CMOStatus) ? model.CMOStatus : string.Empty }
                  , new SqlParameter("DepartmentStatus", SqlDbType.VarChar) { Value = !string.IsNullOrEmpty(model.DepartmentStatus) ? model.DepartmentStatus : string.Empty }
                  , new SqlParameter("ToDate", SqlDbType.NVarChar) { Value = string.IsNullOrEmpty(model.ToDate) ? string.Empty : model.ToDate }
                  , new SqlParameter("FromDate", SqlDbType.NVarChar) { Value = string.IsNullOrEmpty(model.FromDate) ? string.Empty : model.FromDate }
                   , new SqlParameter("CMOOfficerCode", SqlDbType.BigInt) { Value = model.CMOOfficerCode > 0 ? model.CMOOfficerCode : 0 }
                  ).ToList();

                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<SP_JAN_ComplianceModuleAndDeptWiseSummaryReport_Result, ComplianceModuleAndDeptWiseSummaryReportModel>();
                });
                IMapper mapper = config.CreateMapper();
                objReturn = mapper.Map(data, objReturn);

                return SetResultStatus(objReturn, MessageStatus.Success, true);
            }
            catch (Exception ex)
            {
                CreateLogHelper.CreateLogFile("GetCMISComplianceReport ex.Message :" + ex.Message + " \n");
                CreateLogHelper.CreateLogFile("GetCMISComplianceReport ex.InnerException.Message :" + ex.InnerException.Message + " \n");
                CreateLogHelper.CreateLogFile("GetCMISComplianceReport ex.InnerException.InnerException.Message :" + ex.InnerException.InnerException.Message + " \n");
                return SetResultStatus<List<ComplianceModuleAndDeptWiseSummaryReportModel>>(null, MessageStatus.Error, false);
            }
        }

        #endregion

        #region CMIS Achievement Report

        /// <summary>
        /// Get summary report CMIS Achievement which used on jankalyan portal 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ServiceResponse<List<CMISAchievementSummayReportModel>> GetCMISAchievementSummaryReport(CMISAchievementFilterModel model)
        {
            try
            {
                List<CMISAchievementSummayReportModel> objReturn = new List<CMISAchievementSummayReportModel>();
                List<SP_JAN_CMIS_AchievementSummayReport_Result> data = _uow.ExeccuteStoreProcedure<SP_JAN_CMIS_AchievementSummayReport_Result>("SP_JAN_CMIS_AchievementSummayReport @DepartmentCode,@ToDate,@FromDate"
                  , new SqlParameter("DepartmentCode", SqlDbType.Decimal) { Value = model.DepartmentCode > 0 ? model.DepartmentCode : 0 }
                  , new SqlParameter("ToDate", SqlDbType.NVarChar) { Value = string.IsNullOrEmpty(model.ToDate) ? string.Empty : model.ToDate }
                    , new SqlParameter("FromDate", SqlDbType.NVarChar) { Value = string.IsNullOrEmpty(model.FromDate) ? string.Empty : model.FromDate }
                  ).ToList();
                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<SP_JAN_CMIS_AchievementSummayReport_Result, CMISAchievementSummayReportModel>();
                });
                IMapper mapper = config.CreateMapper();
                objReturn = mapper.Map(data, objReturn);

                return SetResultStatus(objReturn, MessageStatus.Success, true);
            }
            catch (Exception ex)
            {
                CreateLogHelper.CreateLogFile("GetCMISAchievementSummaryReport ex.Message :" + ex.Message + " \n");
                CreateLogHelper.CreateLogFile("GetCMISAchievementSummaryReport ex.InnerException.Message :" + ex.InnerException.Message + " \n");
                CreateLogHelper.CreateLogFile("GetCMISAchievementSummaryReport ex.InnerException.InnerException.Message :" + ex.InnerException.InnerException.Message + " \n");
                return SetResultStatus<List<CMISAchievementSummayReportModel>>(null, MessageStatus.Error, false);
            }
        }

        /// <summary>
        /// Get Detail report CMIS Achievement which used on jankalyan portal 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ServiceResponse<List<CMISAchievementDetailReportModel>> GetCMISAchievementDetailReport(CMISAchievementFilterModel model)
        {
            try
            {
                List<CMISAchievementDetailReportModel> objReturn = new List<CMISAchievementDetailReportModel>();
                List<SP_JAN_CMIS_AchievementDetailReport_Result> data = _uow.ExeccuteStoreProcedure<SP_JAN_CMIS_AchievementDetailReport_Result>("SP_JAN_CMIS_AchievementDetailReport @DepartmentCode,@ToDate,@FromDate"
                  , new SqlParameter("DepartmentCode", SqlDbType.Decimal) { Value = model.DepartmentCode > 0 ? model.DepartmentCode : 0 }
                  , new SqlParameter("ToDate", SqlDbType.NVarChar) { Value = string.IsNullOrEmpty(model.ToDate) ? string.Empty : model.ToDate }
                    , new SqlParameter("FromDate", SqlDbType.NVarChar) { Value = string.IsNullOrEmpty(model.FromDate) ? string.Empty : model.FromDate }
                  ).ToList();
                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<SP_JAN_CMIS_AchievementDetailReport_Result, CMISAchievementDetailReportModel>();
                });
                IMapper mapper = config.CreateMapper();
                objReturn = mapper.Map(data, objReturn);

                return SetResultStatus(objReturn, MessageStatus.Success, true);
            }
            catch (Exception ex)
            {
                CreateLogHelper.CreateLogFile("GetCMISAchievementDetailReport ex.Message :" + ex.Message + " \n");
                CreateLogHelper.CreateLogFile("GetCMISAchievementDetailReport ex.InnerException.Message :" + ex.InnerException.Message + " \n");
                CreateLogHelper.CreateLogFile("GetCMISAchievementDetailReport ex.InnerException.InnerException.Message :" + ex.InnerException.InnerException.Message + " \n");
                return SetResultStatus<List<CMISAchievementDetailReportModel>>(null, MessageStatus.Error, false);
            }
        }

        #endregion


    }
}
