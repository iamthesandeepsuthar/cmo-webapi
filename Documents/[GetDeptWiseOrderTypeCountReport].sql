
ALTER PROC [dbo].[GetDeptWiseOrderTypeCountReport] 
@DepartmentCategoryCode int=0
AS
BEGIN 

	DECLARE @Qry NVARCHAR(MAX)=''
DECLARE @Qrys NVARCHAR(MAX)=''
SET @Qry=' SELECT ISNULL(DM.DepartmentCode,0) AS DepartmentCode, DM.DepartmentTitle'
SET @Qrys= ', (SELECT COUNT(Id) FROM tblODR_OrderEntryMaster WHERE DepartmentCode=DM.DepartmentCode AND IsDeleted=0) AS Total'

CREATE TABLE #tempTblOrderType
(
Id INT IDENTITY(1,1),
Code NVARCHAR(10),
Name NVARCHAR(250),
CONSTRAINT PK_tempTblAge PRIMARY KEY (Id)
)

INSERT INTO #tempTblOrderType
SELECT CONVERT(NVARCHAR(10), Code), ReportOrderType FROM tblOrderTypeMaster WHERE IsActive=1 AND IsDelete=0 ORDER BY Name

DECLARE @RowCount INT=0
SELECT @RowCount=COUNT(0) FROM #tempTblOrderType

DECLARE @Index INT=1
WHILE @Index<=@RowCount
BEGIN
DECLARE @OrderTypeCode NVARCHAR(10)
DECLARE @OrderTypeName NVARCHAR(25)
SELECT @OrderTypeCode=Code, @OrderTypeName=Name FROM #tempTblOrderType WHERE Id=@Index

SET @Qry += ', (SELECT COUNT(Id) FROM tblODR_OrderEntryMaster WHERE [Type]='+@OrderTypeCode+' AND DepartmentCode=DM.DepartmentCode AND IsDeleted=0) AS ['+@OrderTypeName+']'

SET @Index=@Index+1
END

SET @Qry += @Qrys+' FROM tblDepartmentMaster DM '
+ ' WHERE DM.DepartmentIsActive=1 AND DM.DepartmentIsDeleted=0 '
+ 'and isnull(DM.DepartmentCategoryCode,0)=' + case when @DepartmentCategoryCode>0 then CONVERT(varchar(100) , @DepartmentCategoryCode)     else 'isnull(DM.DepartmentCategoryCode,0)' end
+ ' ORDER BY DM.DepartmentTitle '

PRINT @Qry
EXEC (@Qry)

DROP TABLE #tempTblOrderType

END 
