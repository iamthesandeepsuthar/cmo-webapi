USE [CMIS_APP]
GO
/****** Object:  StoredProcedure [dbo].[spORD_GetOrderBySearch]    Script Date: 11-12-2019 14:47:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/***************************************************

Created By :Paras 

Script Date : 25-11-2019

Description : Filter data in scheme frontend

exec [spORD_GetOrderBySearch]

****************************************************/

ALTER proc [dbo].[spORD_GetOrderBySearch](
-- Optional Filters for Dynamic Search

	@title				nvarchar(500) = '', 
	@OrderNo			nvarchar(500) = '', 
	@TypeName			bigint = 0, 
	@Date				nvarchar(500) = '', 
	@IndividualBeneficiaryScheme		NVARCHAR(500) = '',
	@BeneficiaryCategory		bigint = 0, 
	@Sector				bigint = 0,
	@DepartmentTitle	bigint= 0,
	@Search			nvarchar(max) = ''
)
as 
begin
 select om.Id,om.Type,otm.Name as TypeName,om.Date,om.OrderNo,om.Title,om.IndividualBeneficiaryScheme,dm.DepartmentTitle,om.DepartmentCode,

STUFF(( SELECT ',' + CAST(ansmtcategory AS varchar(200)) FROM tblODR_BeneficiaryCategoryLookup obc 

inner join tblBeneficiaryCagegory tbc on tbc.cm_ansmtcategoryid=obc.BeneficiaryCategoryCode



where obc.OrderId =om.Id FOR XML PATH('')) , 1, 1, '') AS BeneficiaryCategoryName,



STUFF(( SELECT ',' + CAST(Name AS varchar(200)) FROM tblODR_OrderSectorLookup os 



inner join tblSectorMaster as tsm on tsm.Code=os.SectorCode



where os.OrderId =om.Id FOR XML PATH('')) , 1, 1, '') AS SectorName



FROM tblODR_OrderEntryMaster om



left join tblLookup lkt on om.IssueBy = lkt.Id 



left join tblOrderTypeMaster otm on otm.Code=om.Type



left join tblDepartmentMaster as dm on dm.DepartmentCode=om.DepartmentCode



where om.IsDeleted=0 and om.IsActive=1


and om.Id in (select OrderId from [tblODR_BeneficiaryCategoryLookup] where BeneficiaryCategoryCode = @BeneficiaryCategory or @BeneficiaryCategory=0) 



and om.Title like case when @title !='' then '%' +@title+ '%' else om.Title end



and( om.DepartmentCode =@DepartmentTitle or @DepartmentTitle=0) 



and om.Id in (select OrderId from tblODR_OrderSectorLookup where SectorCode = @Sector or @Sector=0)



and isnull(om.IndividualBeneficiaryScheme,'') like case when @IndividualBeneficiaryScheme !='' then '%' +@IndividualBeneficiaryScheme+ '%' else isnull(om.IndividualBeneficiaryScheme,'') end





and isnull(om.Date,'') like case when @Date !='' then '%' +@Date+ '%' else isnull(om.Date,'') end



and isnull(om.OrderNo,'') like case when @OrderNo !='' then '%' +@OrderNo+ '%' else isnull(om.OrderNo,'') end



and( om.Type =@TypeName or @TypeName=0)	



and isnull(om.SearchCriteria,'') like case when @Search !='' then '%' +@Search+ '%' else isnull(om.SearchCriteria,'') end



end
