//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CMOWebApi.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblADV_RajAdvtPledgeRegister
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public Nullable<int> Gender { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public string PinCode { get; set; }
        public Nullable<int> State { get; set; }
        public Nullable<int> District { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public Nullable<int> OTP { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }
}
