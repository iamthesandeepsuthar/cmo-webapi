﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMOWebApi.Models.ComparetiveModel
{
	public class EBookletFilterModel
	{
		public int DepartmentCode { get; set; }
		public int BeneficiaryCategoryCode { get; set; }
		public int KPICategoryCode { get; set; }
        public long YearWiseCode { get; set; }
        public long GrandTotalCode { get; set; }
        public int GeneralEntryEBookletCode { get; set; }
    }

	public class EBookletResponseModel {
		public string Logo { get; set; }
		public string Name { get; set; }

		public List<BookletDataModel> EBookletList { get; set; }
	}

	public class BookletDataModel
	{
		public string Description { get; set; }
		public string YearName { get; set; }
	}

}
