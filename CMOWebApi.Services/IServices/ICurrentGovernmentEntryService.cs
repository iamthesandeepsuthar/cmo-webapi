﻿using CMOWebApi.Models.ComparetiveModel;
using CMOWebApi.Models.GeneralModel;
using CMOWebApi.Services.ServiceHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMOWebApi.Services.IServices
{
	public interface ICurrentGovernmentEntryService
	{
		/// <summary>
		/// Get all Current Government Entry 
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		ServiceResponse<PagedData<CurrentGovtEntryListViewModel>> GetAll(IndexModel model);

		/// <summary>
		/// Craete new Current Government Entry  
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		Task<ServiceResponse<string>> Create(CurrentGovtEntryModel model);

		/// <summary>
		/// Current Government Entry by Id(Primary key)
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		ServiceResponse<CurrentGovtEntryModel> GetById(long id);

		/// <summary>
		/// Update existing Current Government Entry 
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		Task<ServiceResponse<string>> Edit(CurrentGovtEntryModel model);

		/// <summary>
		/// Update Status(Active/De-Active)
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		Task<ServiceResponse<string>> UpdateActiveStatus(long id);

        /// <summary>
        /// check duplicate record of current gov.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        ServiceResponse<CurrentGovtEntryModel> IsDataAvailable(CurrentGovtEntryModel model);

    }
}
