﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMOWebApi.Core.Enums
{
	public class DepartmentCategory
	{
		public enum DepartmentCategoryEnum
		{
			[StringValue("DepartmentCategory")]
			DepartmentCategory = 1,

			[StringValue("districtfromlookup")]
			District = 30098,

           [StringValue("DistrictFromLookupProduction")]
            DistrictProduction = 30147
        }
	}
}
