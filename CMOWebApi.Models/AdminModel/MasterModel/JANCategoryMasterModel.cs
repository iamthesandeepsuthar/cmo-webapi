﻿using System;

namespace CMOWebApi.Models.AdminModel.MasterModel
{
    public class JANCategoryMasterModel
    {
        JANCategoryMasterModel()
        {
            IsActive = true;
            IsDeleted = false;
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string NameHindi { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
    }
}
