
using CMOWebApi.Models.LoginModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMOWebApi.Models.AutoGeneratedModels
{
    [Table("tblBlogComments")]
    public partial class tblBlogComment
    {
        [Key]
        public long Id { get; set; }
        public long BlogId { get; set; }
        public string UserId { get; set; }
        public Nullable<long> ParentId { get; set; }
        public string Message { get; set; }
        public bool IsApproved { get; set; }
        public System.DateTime CreateDate { get; set; }

       // [ForeignKey("UserId")]
       // public virtual AspNetUser AspNetUser { get; set; }
    }

    [Table("tblBlogComments")]
    public partial class tblBlogCommentRef
    {
        [Key]
        public long Id { get; set; }
        public long BlogId { get; set; }
        [Column("UserId")]
        public string UserId_ref { get; set; }
        public long? ParentId { get; set; }
        public string Message { get; set; }
        public bool IsApproved { get; set; }
        public System.DateTime CreateDate { get; set; }

        [ForeignKey("UserId_ref")]
        public ApplicationUser ApplicationUser { get; set; }
    }
}
