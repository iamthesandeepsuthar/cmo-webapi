
/***************************************************
Created By :Tanmaya 
Script Date : 08-10-2019
Description : Update Status of Monitoring Parameters 
				with their child
exec [spSCM_UpdateMonitoringParametersStatus]
****************************************************/

 alter proc [dbo].[spSCM_UpdateMonitoringParametersStatus]
  @Id bigint=0,
  @Key nvarchar(100)=''
 as
 Begin
 if(LOWER(@Key)=LOWER('PARAMMAPPING'))
 begin
 if (@Id>0)
 begin 
 declare @isActive bit=0
 select @isActive=isnull(IsActive,0) from [tblSCM_MonitoringParameterMapping]  where id=@Id

 update [tblSCM_MonitoringParameterMapping] set IsActive=case when @isActive=0  then 1 else 0 end
 where id=@Id

 update [tblSCM_MonitoringParameterDataEntryFieldValue] 
 set IsActive=case when @isActive=0  then 1 else 0 end where MappingId=@Id

 select 1 as UpdateStatus
 end
 else begin
  select 0 as UpdateStatus
 end
 end
 
  if(LOWER(@Key)=LOWER('ParamValueEntry'))
 begin
 if (@Id>0)
 begin 
 
 update [tblSCM_MonitoringParameterDataEntry] set IsDeleted=1
 where SchemeId=@Id

 update [tblSCM_MonitoringParameterDataEntryFieldValue] 
 set IsDeleted=1  where SchemeId=@Id

 select 1 as UpdateStatus
 end
 else begin
  select 0 as UpdateStatus
 end
 end
 else begin
  select 0 as UpdateStatus
 end
 

end


