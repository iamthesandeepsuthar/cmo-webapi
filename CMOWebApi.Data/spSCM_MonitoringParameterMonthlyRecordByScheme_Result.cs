//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CMOWebApi.Data
{
    using System;
    
    public partial class spSCM_MonitoringParameterMonthlyRecordByScheme_Result
    {
        public string FieldName { get; set; }
        public Nullable<int> MonitoringParamId { get; set; }
        public string FieldValue { get; set; }
        public Nullable<long> DataEntryId { get; set; }
        public string MonthName { get; set; }
        public string FieldDisplayValue { get; set; }
        public long DataEntryFieldValueId { get; set; }
        public int IsEdit { get; set; }
    }
}
