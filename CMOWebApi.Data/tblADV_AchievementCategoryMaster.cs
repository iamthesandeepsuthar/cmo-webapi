//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CMOWebApi.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblADV_AchievementCategoryMaster
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblADV_AchievementCategoryMaster()
        {
            this.tblADV_Achievements = new HashSet<tblADV_Achievements>();
            this.tblADV_AchievementSubCategoryMaster = new HashSet<tblADV_AchievementSubCategoryMaster>();
        }
    
        public int CategoryId { get; set; }
        public int CategoryCode { get; set; }
        public string Title { get; set; }
        public string TitleHindi { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public string ImagePath { get; set; }
        public bool IsVisible { get; set; }
        public Nullable<bool> CategoryIsVisible { get; set; }
        public Nullable<bool> IsVisibleToEndUser { get; set; }
        public string ReportName { get; set; }
        public string HelpFileURL { get; set; }
        public Nullable<bool> IsVisibleDate { get; set; }
        public Nullable<bool> IsVisibleDescriptionHindi { get; set; }
        public string LabelAchievementHindi { get; set; }
        public string LabelDescriptionHindi { get; set; }
        public string LabelDate { get; set; }
        public string LabelURL { get; set; }
        public string LabelAddPDF { get; set; }
        public string LabelAttachImage { get; set; }
        public Nullable<bool> IsShowConnectWithCMIS { get; set; }
        public Nullable<bool> IsShowBeneficiaryCategory { get; set; }
        public Nullable<bool> IsPDFMandatory { get; set; }
        public Nullable<bool> IsURLMandatory { get; set; }
        public Nullable<bool> IsImageMandatory { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblADV_Achievements> tblADV_Achievements { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblADV_AchievementSubCategoryMaster> tblADV_AchievementSubCategoryMaster { get; set; }
    }
}
