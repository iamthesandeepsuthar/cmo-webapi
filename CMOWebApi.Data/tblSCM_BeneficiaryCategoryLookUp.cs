//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CMOWebApi.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblSCM_BeneficiaryCategoryLookUp
    {
        public long Id { get; set; }
        public long schemeId { get; set; }
        public Nullable<long> BeneficiaryCode { get; set; }
    
        public virtual tblSCM_SchemeMaster tblSCM_SchemeMaster { get; set; }
    }
}
