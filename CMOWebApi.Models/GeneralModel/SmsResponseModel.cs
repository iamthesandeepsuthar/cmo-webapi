﻿namespace CMOWebApi.Models.GeneralModel
{
    public class SmsResponseModel
    {
            public int responseCode { get; set; }
            public string responseMessage { get; set; }
            public string responseID { get; set; }
        
    }
}
